#include "Utils.h"

//Pushes a new event to the SDL event-queue
void New_Event(Sint32 code, void* button_flags,void* angle) {
		SDL_Event new_event;
		SDL_zero(new_event);
		new_event.type = SDL_USEREVENT;
		new_event.user.code = code;
		new_event.user.data1 = button_flags;
		new_event.user.data2 = angle;
		SDL_PushEvent(&new_event);
};
