#pragma once
#include "graphicsasset.h"
#include <Box2D\Box2D.h>
#include "Dictionary.h"

class Shot :
	public GraphicsAsset
{
private:
	b2Body* bullet_;
	int time_;
	bool weapon_;
public:
	Shot();
	Shot(int,int,string);
	Shot(const Shot&);
	~Shot(void);
	void set_body(b2Body*);
	void Draw(void);
	void Update(void);
	bool IsMoving(void);
	void destroyBullet(void);
	int get_alive_time();
	void set_body_pos(const int,const int);
	void Deactivate(void);
	void Activate(void);
	void set_velocity(b2Vec2);
	int get_btd_id();
	bool is_weapon();
	void set_weapon(bool);
	void reset_image_id();
};

