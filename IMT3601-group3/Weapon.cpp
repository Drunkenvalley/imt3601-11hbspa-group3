#include "Weapon.h"
//Constructor takes a string to the image of the Weapon, length of the character's
// arm, length of the weapon, amount of ammo, ammo type in index and a pointer
// to the Shot Factory that the world uses.
Weapon::Weapon(string file_name,int arm_lenght,int weapon_lenght,int ammo,int ammo_type, ShotFactory* factory):
	GraphicsAsset(0,0,file_name)
{

	center_.x = this->image_width_/2;
	center_.y = this->image_height_/2; 
	file_name_of_ammo = AMMO_NAME_ONE;
	weapon_lenght_ = weapon_lenght;
	arm_lenght_ = arm_lenght;
	ammo_ = ammo;
	ammo_type_ = ammo_type;
	bullet_factory_ = factory;
}

Weapon::~Weapon(void) 
{
}
//Updates the Weapon to the current time step.
void Weapon::Update(int x,int y) {
	int mouse_x, mouse_y = 0;
	SDL_GetMouseState(&mouse_x,&mouse_y);
	//Mouse uses window coordinates rather than world coordinates.
	Camera* camera_ptr = Camera::getCamera();
	mouse_x = camera_ptr->mouse_x_to_screen(mouse_x);
	mouse_y = camera_ptr->mouse_y_to_screen(mouse_y);

	//Angle calculation between mouse and character.
	mouse_x = x - mouse_x;
	mouse_y = y - mouse_y;
	rotation_ = -atan2(mouse_x,mouse_y) * 180 / M_PI-90;
	//Flips the image of the weapon.
	if (rotation_ > 90.0f || rotation_ < -90.0f) {
		flip_flags_ = SDL_FLIP_HORIZONTAL;
	} else {
		flip_flags_ = SDL_FLIP_NONE;
	}
	//Move the gun to the character with arm offset.
	pos_x_ = x + cos(rotation_*M_PI/180)*arm_lenght_;
	pos_y_ = y + sin(rotation_*M_PI/180)*arm_lenght_+image_height_/2;

}
void Weapon::UpdatePos(int x,int y) {

	//Flips the image of the weapon.
	if (rotation_ > 90.0f || rotation_ < -90.0f) {
		flip_flags_ = SDL_FLIP_HORIZONTAL;
	} else {
		flip_flags_ = SDL_FLIP_NONE;
	}
	//Move the gun to the character with arm offset.
	pos_x_ = x + cos(rotation_*M_PI/180)*arm_lenght_;
	pos_y_ = y + sin(rotation_*M_PI/180)*arm_lenght_+image_height_/2;

}

double Weapon::get_angle() {
	return rotation_;
}
//Sets the angle of rotation.
void Weapon::set_angle(double angle) {
	rotation_ = angle;
}
//Draws the sprite in the currently active SDL Renderer.
void Weapon::Draw(){
	sprites_->DrawSprite(image_id_,pos_x_,pos_y_,rotation_,center_,flip_flags_);
}
//Returns a Shot pointer if the weapon shot a bullet this time frame.
Shot* Weapon::FireBullet() {
	//Create a bullet and return it
	Shot* bullet_ptr = nullptr;
	double radians = get_radians();
	int x = (pos_x_) + cos(radians)*weapon_lenght_;
	int y = (pos_y_) + sin(radians)*weapon_lenght_-image_height_/2;
	//If you have ammo left.
	if (ammo_ > 0){
		bullet_ptr = bullet_factory_->nextShot(x,y);
		
	}//Else we use the weapon as the ammo!
	else if(ammo_ == 0){
		bullet_ptr = bullet_factory_->nextShot(x,y,image_id_);
	} else {
		return nullptr;
	}
	//set position and speed of new bullet
	bullet_ptr->set_velocity(b2Vec2(cos(radians)*10,sin(radians)*10));
	bullet_ptr->set_pos(x,y);
	ammo_--;

	return bullet_ptr;
}
//Returns a used bullet to the factory.
void Weapon::ReturnBullet(Shot* bullet)
{
	bullet_factory_->returnBullet(bullet);
}
//Convert the rotation into radians.
double Weapon::get_radians(){
	return (rotation_*M_PI/180);
}

int Weapon::get_ammo(){
return ammo_;
};
