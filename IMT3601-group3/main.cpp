#include <Windows.h>
#include <SDL.h>
#include <SDL_image.h>
#include <Box2D/Box2D.h>
#include <iostream>
#include <string>
#include "Dictionary.h"
#include "Audio.h"
#include "Config.h"
#include "PlayerController.h"
#include "PhysicsController.h"
#include "GameController.h"
#include "Renderer.h"
#include "MainWindow.h"
using namespace std;




int main(int argc, char *argv[]){

	if (SDL_Init(SDL_INIT_EVERYTHING) == -1){
		return 1;
	}
	/*
	//If something is passed as a command line
	for (int i = 1; i < argc; i++)
	{
		string arg_str = argv[i];
		//Atoi seems to make it null when I send it a path to a file
		int arg_num = atoi(argv[i]);
		//			printf("%s\n%s\n", argv, argument.c_str());
		printf("%s\n", argv[i]);
		if (arg_str.substr(arg_str.find_last_of(".") +1) == "txt")
		{
			printf("It's a file!\n");
		}
		else if(arg_num)
		{
			printf("It's numbers!\n");
		}
	}
	*/
	/*Declaring essential variables*/
	MainWindow* main_window_instance_ptr = MainWindow::getMainWindow();
	
	
	SDL_Event events;
	bool running = true; //exit match loop
	int menuReturns = 1; //match type
	bool quit = false; //exit program

	/*Initializing classes*/
	Config config(CONFIG_FILE); 

	/*Loading from config*/
	const int ScreenWidth = config.GetSetting("Window","win_wide",400);
	const int ScreenHeight = config.GetSetting("Window","win_tall",300);

	Uint32 ScreenFlags = SDL_WINDOW_SHOWN;
	if(config.GetSetting("Window","fullscreen")) {
		ScreenFlags |= SDL_WINDOW_FULLSCREEN;
	}
	if(config.GetSetting("Window","borderless")) {
		ScreenFlags |= SDL_WINDOW_BORDERLESS;
	}
	if(config.GetSetting("Window","maximized")) {
		ScreenFlags |= SDL_WINDOW_MAXIMIZED;
	}
	if(config.GetSetting("Window","desktop")) {
		ScreenFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
	}

	/*Creating environment*/
	if(!main_window_instance_ptr->makeWindow("Hello World!", 100, 100, ScreenWidth, ScreenHeight, ScreenFlags)){
		return 2;
	}

	Camera* camera_ptr = Camera::getCamera();
	camera_ptr->set_screen_size(ScreenWidth,ScreenHeight);
	camera_ptr->Menu_Mode();
	Renderer* renderer = Renderer::getRenderer();
	renderer->setRenderer(main_window_instance_ptr->get_main_window_object(), -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);

	GameController* ControllerOfGame = GameController::getGameController();




	Uint32 startTick = SDL_GetTicks();
	Uint32 endTick = SDL_GetTicks();


	/*Game's loop*/
	menuReturns = ControllerOfGame->TitleMenu();
	quit = bool(!menuReturns);
	while(!quit){
		ControllerOfGame->set_logfile(LOG_FILE);
		SDL_ShowCursor(0);
		switch(menuReturns){
		case 0: quit = true;
		case 1:
			while(running) {
				startTick = SDL_GetTicks();
				//If there is a SDL quit command in the events pool we return false.
				running = ControllerOfGame->HandleEvents(events);
				//Updates and renders
				if(running){
					ControllerOfGame->GameStage(); //run local game
				}
				endTick = (SDL_GetTicks() - startTick);
				SDL_SetWindowTitle(main_window_instance_ptr->get_main_window_object(), std::to_string(endTick).c_str());
			}
			break;
		case 2: 
			while(running) {
				startTick = SDL_GetTicks();
				//If there is a SDL quit command in the events pool we return false.
				running = ControllerOfGame->HandleEvents(events);
				//Get the network packets
				ControllerOfGame->GetNetworkPackets();
				//Updates and renders
				ControllerOfGame->GameStage(); //run network game
				
				endTick = (SDL_GetTicks() - startTick);
				SDL_SetWindowTitle(main_window_instance_ptr->get_main_window_object(), std::to_string(endTick).c_str());
			}
			break;
		default :
			quit = true;
		}

		SDL_ShowCursor(1);
		running = true;
		camera_ptr->Menu_Mode(); //sets camera to menumode
		menuReturns = ControllerOfGame->TitleMenu();
		quit = bool(!menuReturns);
	}

	/*Cleaning up*/

	delete renderer;
	

	SDL_Quit();
	return 0;
}


