// Based on http://www.iforce2d.net/b2dtut/jumpability
#include "BtdContactListener.h"
//Empty constructor.
BtdContactListener::BtdContactListener(void) {
	///Sets all values in the foot sensor collisions to 0.
	for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
	{
		foot_sensor_collisions_[i] = 0;
	}
}

BtdContactListener::~BtdContactListener(void){
}

//Registers that two fixtures have begun contacting eachother.
void BtdContactListener::BeginContact(b2Contact* contact) {
	void* fixtureUserDataA = contact->GetFixtureA()->GetUserData();
	void* fixtureUserDataB = contact->GetFixtureB()->GetUserData();

	for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
	{
		//check if fixture A was the foot sensor
		if ((int)fixtureUserDataA == i)
			foot_sensor_collisions_[i]++;
		//check if fixture B was the foot sensor
		if ((int)fixtureUserDataB == i)
			foot_sensor_collisions_[i]++;
		//	CHECK IF ONE WAS A BULLET AND ONE WAS A PLAYER
		if ((int)fixtureUserDataA == i + NUMBER_OF_PLAYERS ){ //characters  body
			if ((int)fixtureUserDataB >= FIXTURE_OFFSET_TO_SHOTS ) { //shots body
				temp_char_hit_event_.bullet_id_ = (int)fixtureUserDataB;// - FIXTURE_OFFSET_TO_SHOTS; 
				temp_char_hit_event_.char_id_ = i;
				char_hit_event_list_.push_back(temp_char_hit_event_);
			}
			if ((int)fixtureUserDataB == i + NUMBER_OF_PLAYERS ) {//characters  body
				if ((int)fixtureUserDataA >= FIXTURE_OFFSET_TO_SHOTS) {//shots body
				
					temp_char_hit_event_.bullet_id_ = (int)fixtureUserDataB;// - FIXTURE_OFFSET_TO_SHOTS; 
					temp_char_hit_event_.char_id_ = i;
					char_hit_event_list_.push_back(temp_char_hit_event_);
				}
			}
		}
	}
}
//Registers that contact has ended and counts down the contact array.
void BtdContactListener::EndContact(b2Contact* contact) {
	for (int i = 0; i < NUMBER_OF_PLAYERS; i++)
	{
		//Check if fixture A was the foot sensor.
		void* fixtureUserData = contact->GetFixtureA()->GetUserData();
		if ((int)fixtureUserData == i)
			foot_sensor_collisions_[i]--;
		//Check if fixture B was the foot sensor.
		fixtureUserData = contact->GetFixtureB()->GetUserData();
		if ((int)fixtureUserData == i)
			foot_sensor_collisions_[i]--;
	}
}
//Returns the number of collisions in the foot sensor for index.
int BtdContactListener::get_foot_sensor_collisions(int index) {
	return foot_sensor_collisions_[index];
}
//Fills the passed vector with all CharHitEvent's and empties the event list.
void BtdContactListener::GetCharHitEvents(std::vector<CharHitEvent> & s)
{
	s = char_hit_event_list_;
	char_hit_event_list_.clear();
}
