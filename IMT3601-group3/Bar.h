#pragma once
#include "graphicsasset.h"
class Bar :
	public GraphicsAsset
{
private:
	//Current value of the bar, max 300.
	int value_;
	float max_ammo_;
	float ammo_;
	int image_id_ammo_;
	int image_id_ammo_empty_;
	//Image ID's for the different parts of the bar.
	int image_id_green_;
	int image_id_red_;
	int image_id_blue_;
	//Pointers to the partial and full images.
	int* image_id_ptr_partial_;
	int* image_id_ptr_full_;
public:
	//Empty constructor.
	Bar();
	//Constructor that takes a top left x and y followed by a string to the filename.
	Bar(int,int,string);
	~Bar(void);
	//Drawing function that takes the position of where the Bar is this timestep.
	void Draw(float,float);
	//Sets the value of the bar to the number sent.
	void set_value_ammo(int ammo);
	void set_value_hp(int hp);
	void set_max_ammo(int new_max_ammo);
};

