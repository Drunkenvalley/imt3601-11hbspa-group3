#pragma once
#include <Box2D\Box2D.h>
#include "GraphicsAsset.h"
class Scenery :
	public GraphicsAsset
{
private:
	b2Body* platform_body_;
	int width_;
	int height_;

public:
	//Constructor that takes a x and y of the top left corner and filename of sprite
	Scenery(int,int,string);
	//Constructor that takes the top left x and y followed by the width and height.
	Scenery(int,int,int,int);
	~Scenery(void);
	//Returns a pointer to the Box2D body object.
	b2Body* get_b2body(void);
	//Returns the value of the top left x.
	int get_pos_x(void);
	//Returns the value of the top left y.
	int get_pos_y(void);
	//Returns the width of the Scenery.
	int get_width(void);
	//Returns the height of the Scenery.
	int get_height(void);
	//Sets the Box2D body object.
	void set_b2body(b2Body* body);
	void Draw(void);
};

