#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <Box2D\Box2D.h>
#include <vector>
#include <fstream>

//Stage specific objects
#include "Scenery.h"
#include "Shot.h"
#include "Weapon.h"
#include "Character.h"
#include "Item.h"
#include "Bar.h"

using namespace std;

struct stage_objects {
		vector<Scenery> plattforms_;
		vector<Item> pickups_;
		vector<bool> pickups_positions_; //check if item spawnpoints are filled
		vector<Character> agents_;
		vector<b2Vec2> spawn_point_items_;
		vector<b2Vec2> spawn_point_chars_;
		GraphicsAsset* background_;
		vector<Shot*> bullets_;
		bool stage_loaded_flag;
		string stage_file_;
};

class World
{
private:
	World(void);
	static World* world_instance_;
	stage_objects stage_;
public:
	static World* get_world();
	stage_objects* get_stage();
	~World(void);
	bool LoadStage(string filename);
	void UnloadStage(void);
	bool StageIsLoaded(void);
};

