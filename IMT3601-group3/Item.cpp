#include "Item.h"
#define CHARACTER_ITEM_REACH 60
//Constructor that takes the position and sprite file location along with the type of item.
Item::Item(int x_pos,int y_pos, string file_name,int type):GraphicsAsset(x_pos,y_pos,file_name)
{
	item_type_ = type;
}

Item::~Item(void)
{
}
//Returns true if a charcter can reach this(Item) from position (x_pos,y_pos).
bool Item::CheckDistance(float x_pos,float y_pos)
{
	float x_distance = abs(x_pos - pos_x_);
	float y_distance = abs(y_pos - pos_y_);
	return sqrt(x_distance*x_distance+y_distance*y_distance) < CHARACTER_ITEM_REACH;
}

int Item::get_item_type(){
	return item_type_;
}



