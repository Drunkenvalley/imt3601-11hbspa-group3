#include "GraphicsAsset.h"
//Constructor takes starting position and a string to the sprite image.
GraphicsAsset::GraphicsAsset(int x_position, int y_position, string file_name)
{
	sprites_ = Sprites::getSpritesInstance();
	pos_x_ = x_position;
	pos_y_ = y_position;
	//Saves the ID of the sprite localy.
	image_id_ = sprites_->get_sprite_id(file_name);
	//Stores the size of the sprite localy for easy retrieval.
	image_width_ = sprites_->get_width(image_id_);
	image_height_ = sprites_ -> get_height(image_id_);
}

GraphicsAsset::~GraphicsAsset(void)
{
}
//Sets the position of the asset.
void GraphicsAsset::set_pos(float x_pos, float y_pos) {
	pos_x_ = x_pos;
	pos_y_ = y_pos;
}
void GraphicsAsset::DrawAsBackGround()
{
	sprites_->DrawAsBackground(image_id_);
}

//Renders the asset in the currently active renderer.
void GraphicsAsset::Draw()
{
	sprites_->DrawSprite(image_id_, pos_x_, pos_y_);
}
//Draws the sprite to an offset.
void GraphicsAsset::Draw(float x_pos, float y_pos)
{
	sprites_->DrawSprite(image_id_, pos_x_ + x_pos, pos_y_ + y_pos, rotation_);
}
//Draws the asset to an offset with a rotation angle.
void GraphicsAsset::Draw(float pos_x, float pos_y, float rotation)
{
	sprites_->DrawSprite(image_id_, pos_x_ + pos_x, pos_y_ + pos_y, rotation_ + rotation);
}


// NB! make sure the image of that id is loaded;
	void GraphicsAsset::set_image_id(int new_id){
		image_id_ = new_id;

	}



//Sets the angle of rotation.
void GraphicsAsset::set_rotation(double rotation)
{
	rotation_ = rotation;
}
//Adds sent rotation to the current angle.
void GraphicsAsset::add_rotation(double rotation)
{
	rotation_ += rotation;
}
//Sets the horizontal flip flag.
void GraphicsAsset::set_flip_horizontal(bool horizontal_flip)
{
	flip_horizontal = horizontal_flip;
}
//Sets the vertical flip flag.
void GraphicsAsset::set_flip_vertical(bool vertical_flip)
{
	flip_vertical = vertical_flip;
}
//Toggles the vertical flip.
void GraphicsAsset::FlipVertical()
{
	flip_vertical = flip_vertical?false:true;
}
//Toggels the horizonta flip.
void GraphicsAsset::FlipHorizontal()
{
	flip_horizontal = flip_horizontal?false:true;
}

int GraphicsAsset::get_img_index(){
	return image_id_ ;
}

int GraphicsAsset::get_img_width(){
	return image_width_;
}

int GraphicsAsset::get_img_height(){
	return image_height_;
}

float GraphicsAsset::get_pos_x(){
	return pos_x_;
}

float GraphicsAsset::get_pos_y(){
	return pos_y_;
}
