#include "Dictionary.h"
#include "Character.h"

//Empty constructor
Character::~Character(){

};
//Full constructor that thakes the starting position of the character, a pointer to the physics,
// a predeclared character type, a unique ID and lastly a pointer to the ShotFactory.
Character::Character(float x_pos, float y_pos, PhysicsController* physics_controller, int char_type,int char_id, ShotFactory* factory){
	//Sets local singeltons.
	//is_ghost_ = false;
	sprite_container_ = Sprites::getSpritesInstance();
	hp_ = 280;
	hp_bar_= new Bar(x_pos,y_pos,"../Assets/hpbarBlack.png");
	grave_sprite_index_ = sprite_container_->get_sprite_id("../Assets/Grave.png");

	audio_ = Audio::getInstance();
	//Sets the bullet factory.

	bullet_factory_ = factory;
	//Sets the HP bar of the charater.
	hp_bar_= new Bar(x_pos,y_pos,"../Assets/hpbarBlack.png");
	//Sets the weapon.
	set_weapon(char_type);
	//Starting position of the charater.
	x_position_ = x_pos/BOX_2D_SCALING;
	y_position_ = y_pos/BOX_2D_SCALING;
	//Sets the physics controller and phyisc related things.
	physics_controller_ = physics_controller;
	foot_sensor_collitions_ = 0;
	//Defaults the inputs to be all false.
	move_jump_ = false;
	move_left_ = false;
	move_right_ = false;
	move_down_ = false;
	weapon_fired_ = false;
	// Used to identify collisions. must be unique.
	character_id_ = char_id;
	character_type_ = char_type;

	switch(char_type){
	case 0:
		spriteIndex_ = sprite_container_->get_sprite_id("../Assets/movingbox.png");
		bodyparts_.push_back(GraphicsAsset(x_pos, y_pos, "../Assets/movingbox.png"));
		jump_force_ = 6;
		power_ = 3;
		hp_ = 280;
		break;
	case 1:	
		spriteIndex_ = sprite_container_->get_sprite_id("../Assets/movingbox2.png");
		bodyparts_.push_back(GraphicsAsset(x_pos, y_pos, "../Assets/movingbox2.png"));
		jump_force_ = 5;
		power_ = 4;
		hp_ = 300;
		break;
	case 2:	
		spriteIndex_ = sprite_container_->get_sprite_id("../Assets/movingbox3.png");
		bodyparts_.push_back(GraphicsAsset(x_pos, y_pos, "../Assets/movingbox3.png"));
		jump_force_ = 7;
		power_ = 2;
		hp_ = 299;
		break;
	}
	//If we got a Sprites instance.
	if (spriteIndex_ != -1){
		width_ = float(sprite_container_->get_width(spriteIndex_)/BOX_2D_SCALING);
		height_ = float(sprite_container_->get_height(spriteIndex_)/BOX_2D_SCALING);
	} else {
		width_ = 1;
		height_ = 1;
	}
	/*
	*	Definitions for the Box2D character
	*	Do not touch unless you know what you are doing.
	*/
	//Filter
	b2Filter filter;
	//Defines the body
	b2BodyDef characterBodyDef;
	characterBodyDef.type = b2_dynamicBody;
	characterBodyDef.position.Set(x_position_, y_position_);
	characterBodyDef.fixedRotation = true;
	//Adds it to the Box2D world
	btdBody_ = physics_controller_->AddBody(&characterBodyDef);
	//Sets the shape of the character to be a box.
	b2PolygonShape characterBox;
	characterBox.SetAsBox(float(width_/2.0), float(height_/2.0));
	//Defines the fixture and physical properties of the character.
	b2FixtureDef characterFixtureDef;
	characterFixtureDef.shape = &characterBox;
	characterFixtureDef.density = (width_*height_);
	characterFixtureDef.friction = 1.0f;
	body_fixture_ = btdBody_->CreateFixture(&characterFixtureDef);
	//Gives the character body a unique ID, used for hit detecions.
	body_fixture_->SetUserData((void*) (char_id + NUMBER_OF_PLAYERS));
	filter = body_fixture_->GetFilterData();
	filter.groupIndex = 2;
	body_fixture_->SetFilterData(filter);
	//Foot sensor used to check if you are jumping.
	b2PolygonShape footSensorBox;
	footSensorBox.SetAsBox(float((width_-0.1)/2.0), float(0.01), b2Vec2(0, + float(height_/2.0)), 0);
	b2FixtureDef footSensorFixtureDef;
	footSensorFixtureDef.shape = &footSensorBox;
	footSensorFixtureDef.density = 0;
	footSensorFixtureDef.isSensor = true;
	foot_sensor_fixture_ = btdBody_->CreateFixture(&footSensorFixtureDef);
	foot_sensor_fixture_->SetUserData((void*)character_id_);
	footSensorFixtureDef.filter.groupIndex = -2;
}

//Updates the character to the current timeframe and checks if it hits a
// pick up item sent in the list.
void Character::Update(vector<Item>* list_of_pickups) {
	//Updates the position to the the current timeframe.
	b2Vec2 position = btdBody_->GetPosition();
	x_position_ = position.x*BOX_2D_SCALING;
	y_position_ = position.y*BOX_2D_SCALING;

	if(character_active_) {

	//Gets the number of colissions that occured in the foot sensor.

	foot_sensor_collitions_ = physics_controller_->get_foot_sensor_collisions(character_id_);
	//If the down button was pressed this frame.
	if(move_down_) {
		//Loops trough the list of items to see if you intersected with it.
		for (int i = 0 ; i< (*list_of_pickups).size();i++){
			if((*list_of_pickups)[i].CheckDistance(x_position_,y_position_)){
				set_weapon((*list_of_pickups)[i].get_item_type());
				//Erases the item you intersected with.
				(*list_of_pickups).erase((*list_of_pickups).begin()+i); 
				//No need to pick up two items at once.
				break;
			}
		}
	}
	//If you pressed the jump button.
	if(move_jump_) {
		if(btdBody_->GetLinearVelocity().y == 0 && foot_sensor_collitions_ > 0) {
			//jump_force_ should be 4-6-ish
			btdBody_->ApplyLinearImpulse(b2Vec2(0,-btdBody_->GetMass()*jump_force_), btdBody_->GetWorldCenter()); 
			audio_->PlaySound(JUMP_SOUND);
		}
	}
	//If you pressed left and not right.
	if(move_left_ && !move_right_) {
		btdBody_->SetLinearVelocity(b2Vec2(-3,btdBody_->GetLinearVelocity().y));
		body_fixture_->SetFriction(0.0f);
	} 
	//If you pressed right and not left.
	else if(move_right_ && !move_left_) {
		btdBody_->SetLinearVelocity(b2Vec2(3,btdBody_->GetLinearVelocity().y));
		body_fixture_->SetFriction(0.0f);
	}
	//Else we stop you.
	else {
		// decrease x-velocity slightly
		btdBody_->SetLinearVelocity(b2Vec2(btdBody_->GetLinearVelocity().x * SLIGHT_DECREASE, btdBody_->GetLinearVelocity().y));
		
		//body_fixture_->SetFriction(1.0f); // <--doesn't work
	}
	//Loops trough the body parts and updates them.
	for (int i = 0; i < bodyparts_.size(); i++){
		bodyparts_[i].set_rotation(int(x_position_)%360);
		bodyparts_[i].set_pos(0, 0);
	}
	//Update weapons position and rotation.
	if(weapon_){
		if(character_id_ == 0)
			weapon_->Update(x_position_,y_position_); //with rotation
		else
			weapon_->UpdatePos(x_position_,y_position_); //without rotation
	}
	//Updates the HP bar.
	hp_bar_->set_value_hp(hp_);
	//update ammo bar
	if(weapon_){
		hp_bar_->set_value_ammo(weapon_->get_ammo());
	}
	//Restet input flags
	//input_ = 0x0; // !!!
	}
}

void Character::Update() {
	//If you pressed the jump button.
	if(move_jump_) {
		/*if(btdBody_->GetLinearVelocity().y == 0 && foot_sensor_collitions_ > 0) {
			audio_->PlaySound(JUMP_SOUND);
		}*/
	}

	//Loops trough the body parts and updates them.
	for (int i = 0; i < bodyparts_.size(); i++){
		bodyparts_[i].set_rotation(int(x_position_)%360);
		bodyparts_[i].set_pos(0, 0);
	}
	//Update weapons position and rotation.
	if(weapon_){
		if(character_id_ == 0)
			weapon_->Update(x_position_,y_position_); //with rotation
		else
			weapon_->UpdatePos(x_position_,y_position_); //without rotation
	}
}




//Updates the input flags.
void Character::InputUpdate(Uint32 input) {
	//For button down and things begin.
	if(input&JUMP) move_jump_ = true;
	if(input&LEFT) move_left_ = true;
	if(input&RIGHT) move_right_ = true;
	if(input&DOWN) move_down_ = true;
	if(input&SHOOT) weapon_fired_ = true;
	//For button up and things end.
	if(input >>16 &JUMP) move_jump_ = false;
	if(input >>16 &LEFT) move_left_ = false;
	if(input >>16 &DOWN) move_down_ = false;
	if(input >>16 &RIGHT) move_right_ = false;
	if(input >>16 &SHOOT) weapon_fired_ = false;
	//Sets the input flags.
	input_ = input;
}

void Character::InputUpdate(Uint32 input,double angle) {
	InputUpdate(input);
	input_ = input;
	//Rotates weapon if there is one.
	if(weapon_){
		weapon_->set_angle(angle);
	}
}
//Draw the character in the current renderer.
void Character::draw(){

	if(character_active_) {
	sprite_container_->DrawSprite(spriteIndex_, x_position_, y_position_);
	hp_bar_->Draw(x_position_,y_position_);
	//Draws each of the body parts.
	for(int i = 0; i < bodyparts_.size();i++)
		bodyparts_[i].Draw(x_position_, y_position_);
	//Draws the weapon if there is one.
	if(weapon_){
		weapon_->Draw();
	}
	}
	else {
		sprite_container_->DrawSprite(grave_sprite_index_, x_position_, y_position_);
	}
}

Uint32 Character::get_input_flags(){
	return input_;
}

void Character::set_weapon(int type){
	int ammo;
	switch (type)
	{
	case 0:
		ammo = 27;
		weapon_ = new Weapon("../Assets/weapon.png",25,30,ammo,1, bullet_factory_);
		break;
	case 1:
		ammo = 15;
		weapon_ = new Weapon("../Assets/weapon2.png",55,35,ammo,1, bullet_factory_);
		break;
	case 2:
		ammo = 37;
		weapon_ = new Weapon("../Assets/weapon3.png",-25,120,ammo,1, bullet_factory_);
		break;
	default:
		ammo = 7;
		weapon_ = new Weapon("../Assets/weapon3.png",-25,120,ammo,1, bullet_factory_);
		break;
	}
	hp_bar_ ->set_max_ammo(ammo);
};


int Character::get_character_type(void){
	return character_type_;
}

float Character::get_x_pos(){
	return x_position_;
}

float Character::get_y_pos(){
	return y_position_;
}

void Character::set_x_pos(int pos_x){
	x_position_ = pos_x;
}

void Character::set_y_pos(int pos_y){
	y_position_ = pos_y;
}

float Character::get_width()
{return width_;
}

float Character::get_height(){
	return height_;
}

double Character::get_angle(){
	if(weapon_) {
		return weapon_->get_angle();
	}
	return 0;
}

void Character::set_rotation(double angle){
	if(weapon_) {
		weapon_->set_rotation(angle);
	}
}

//Returns a Shot pointer to put in the world vector of bullets.
Shot* Character::FireBullet() {
	//If weapon was fireed this frame and you have a weapon.
	if(weapon_fired_ && weapon_ && weapon_delay_ < 1)
	{
		Shot* shot_ptr = weapon_->FireBullet();
		if (shot_ptr->is_weapon()){ 
			weapon_ = nullptr;
			weapon_delay_ = 0;
			if (shot_ptr == nullptr ){
				return nullptr;
			}
		};
		audio_->PlaySound("../Assets/67045drminkyslimejump.wav");
		weapon_delay_ = 20 - power_;
		return shot_ptr;
	}else{
		weapon_delay_ --;
		return nullptr;
	}
}
//Update the HP of the character by subtracting sent damage.
bool Character::UpdateHp(int dmg) {
	hp_ -= dmg;
	if(hp_ > 0)
		return true;
	else {
		character_active_ = false;
		btdBody_->SetActive(false);
		return false;
	}
}

bool Character::is_active() {return character_active_;}; //is alive


