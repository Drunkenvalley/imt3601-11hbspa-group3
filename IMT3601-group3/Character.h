#pragma once
#include <vector>
#include <Box2D\Box2D.h>
#include "GraphicsAsset.h"
#include "PhysicsController.h"
#include "Sprites.h"
#include "Dictionary.h"
#include "Audio.h"
#include "Weapon.h"
#include "Shot.h"
#include <cmath>
#include "Bar.h"
#include "Item.h"
#include "ShotFactory.h"

class Character
{
private:
	//Vector of all the body parts of the character.
	vector<GraphicsAsset> bodyparts_;
	//Pointer to the weapon currently used by the character.
	Weapon* weapon_;
	//Pointer to a audio object makes the character able to play sounds.
	Audio* audio_;
	//Pointer to the bullet factor for shooting bullets.
	ShotFactory* bullet_factory_;
	Sprites* sprite_container_;
	int spriteIndex_;
	int grave_sprite_index_;
	int character_type_;
	//Information about the size and position of the Character.
	float width_;
	float height_;
	float x_position_;
	float y_position_;
	//HP counter.
	int hp_;
	bool character_active_; // character is /not dead;
	//Pointer to the HP bar.
	Bar* hp_bar_;
	//Variable for how much force there is in the jump of a character.
	int jump_force_;
	//JAKOB DOES NOT KNOW, HIT HIM.
	int power_; //gun power or defence?
	int weapon_delay_;
	//Bools define how the character moves this time step.
	bool move_jump_;
	bool move_down_;
	bool move_left_;
	bool move_right_;
	//32 Bits of input knowledge.
	Uint32 input_;
	//True if the weapon is to be fired.
	bool weapon_fired_;
	//Unique ID for the character that is coupled with the foot sensor ID.
	int character_id_;
	int foot_sensor_collitions_;
	//The Box2D object
	PhysicsController *physics_controller_;
	b2Body* btdBody_;
	b2Fixture *body_fixture_;
	b2Fixture *foot_sensor_fixture_;
	//bool is_ghost_;
public:
	//Constructor that takes the x position, y position, a pointer to the
	// physics controller, character type, the character ID and a pointer to a ShotFactory
	Character(float,float,PhysicsController*,int,int,ShotFactory*);
	~Character(void);
	//Returns a pointer to the bullet if the weapon shot, null if not.
	Shot* FireBullet(void);
	//Return functions for positions and size of the Character.
	float get_x_pos(void);
	float get_y_pos(void);
	float get_width(void);
	float get_height(void);
	double get_angle(void);
	int get_character_type(void);
	//Return function for the Uint32 input flags.
	Uint32 get_input_flags(void);
	//Returns the index of the sprite.
	int get_sprite(void);
	//Decreases the HP of the Character, returns false if character is dead.
	bool UpdateHp(int dmg);
	//Drawing function for the Character.
	void draw(void);
	//Updates the character for this timestep, takes a vector of items to see if the
	// character is to pick up one of them.
	void Update(vector<Item>*);
	void Update();
	//Updates the Uint32 input flags to the flags passed.
	void InputUpdate(Uint32);
	//Updates the Uint 32 input flags to the flags passed and the angle to the
	// angle passed.
	void InputUpdate(Uint32,double);
	//Sets a new weapon to the int that is passed.
	void set_weapon(int);
	//Changes the sprite to the sprite number passed.
	void set_sprite(int);
	//Sets the position to the value passed.
	void set_x_pos(int x_pos);
	void set_y_pos(int y_pos);
	void set_rotation(double angle);
	bool is_active();
};
