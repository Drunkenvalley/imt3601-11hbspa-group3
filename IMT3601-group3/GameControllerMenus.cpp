#include "GameController.h"

int GameController::StageSelect(){
	int selected_stage = MAXINT;
	SDL_Event events;
	int mouse_x;
	int mouse_y;
	//Camera* camera = Camera::getCamera();

	while (selected_stage > stage_menu_.buttons_.size()){
		//draw menu

		/*Updating renderer*/
		SDL_RenderClear(renderer_);
		//Draws the background, nice and easy
		stage_menu_.background_->DrawAsBackGround();
		for (int num_buttons = 0; num_buttons < stage_menu_.buttons_.size(); num_buttons++)
		{
			stage_menu_.buttons_[num_buttons].Draw();
		}
		for (int num_texts = 0; num_texts < stage_menu_.texts_.size(); num_texts++)
		{
			stage_menu_.texts_[num_texts].Draw();
		}
		SDL_RenderPresent(renderer_);

		while(SDL_PollEvent(&events)){

			if(events.type == SDL_MOUSEBUTTONDOWN){
				SDL_GetMouseState(&mouse_x,&mouse_y);
				//mouse_x =camera->mouse_x_to_screen(mouse_x);//
				//mouse_y =camera->mouse_y_to_screen(mouse_y);
				for(int i = 0; i < stage_menu_.buttons_.size(); i++){
					if(stage_menu_.buttons_[i].HitTest(mouse_x,mouse_y))
					{
						selected_stage = i;
					};
				};
				break;
			}
		}

	}

	return selected_stage;
}
int GameController::TitleMenuButtonActions(int id){
	int selected_stage = -1;
	int selected_character = -1;

	int opponent_type;
	b2Vec2 spawnpoint;
	Character* new_player;

	char* ip_address = "127.000.000.001";
	Uint16 port_number = default_client_port_;
	Uint16 server_port_number = default_server_port_;
	char host_check = 'N';

	switch(id) {
	case 0 : 
		selected_stage = StageSelect();
		selected_character = CharacterSelect();
		network_controller_ = NULL;

		load_selected_stage(selected_stage);
		load_selected_character(selected_character);
		// AI agent
		spawnpoint = stage_->spawn_point_chars_[rand()%stage_->spawn_point_chars_.size()];
		new_player = new Character(spawnpoint.x , spawnpoint.y, physics_controller_, rand()%NUMBER_OF_WEAPONS, stage_->agents_.size(), factory_);
		stage_->agents_.push_back(*new_player);	
		return 1; //do local battle

		break;
	case 1 :
		printf("Hosting(y/N): ");
		host_check = cin.get();
		cin.clear();
		cin.ignore(10000, '\n');
		std::cout << host_check;
		if (host_check == 'y' || host_check == 'Y') {
			printf("\n You are the game host.\n");
			network_controller_ = Network::get_instance(port_number, ip_address, server_port_number, true); // !!! make new 
			
			
			selected_stage = StageSelect();
			network_controller_->set_stage_id(selected_stage);
			selected_character = CharacterSelect();

		} else {
			printf("\n You are not the host.\n");
			network_controller_ = Network::get_instance(port_number, ip_address, server_port_number, false);
		

			selected_character = CharacterSelect();
			network_controller_->client_handshake(selected_character);
			while(selected_stage == -1) {
				selected_stage = network_controller_->get_stage();
				network_controller_->send_critical_packets();
				network_controller_->get_packet();
				Sleep(100);
			}
			
			

			
		}

		load_selected_stage(selected_stage);
		network_controller_->set_externals(stage_, factory_, physics_controller_);
		load_selected_character(selected_character);

		return 2; //do network Battle 
		break;
	case 2 : OptionsMenu();return INT_MAX; // do nothing
	case 3 : 
		return 0; //exit game
	}

	

	return 0;
}

int GameController::TitleMenu() //start up menu
{
	SDL_Event events;
	int mouse_x;
	int mouse_y;
	int runModeReturnValue = MAXINT;
	Camera* camera = Camera::getCamera();

	while (runModeReturnValue > title_menu_.buttons_.size()){
		while(SDL_PollEvent(&events)){

			if(events.type == SDL_MOUSEBUTTONDOWN){
				SDL_GetMouseState(&mouse_x,&mouse_y);
				//mouse_x =camera->mouse_x_to_screen(mouse_x);
				//mouse_y =camera->mouse_y_to_screen(mouse_y);
				for(int i = 0; i < title_menu_.buttons_.size(); i++){
					if(title_menu_.buttons_[i].HitTest(mouse_x,mouse_y))
					{
						runModeReturnValue = TitleMenuButtonActions(i);
					};
				};
				break;
			}

			//draw tittlemenu

			/*Updating renderer*/
			SDL_RenderClear(renderer_);
			//Draws the background, nice and easy
			title_menu_.background_->DrawAsBackGround();
			for (int num_buttons = 0; num_buttons < title_menu_.buttons_.size(); num_buttons++)
			{
				title_menu_.buttons_[num_buttons].Draw();
			}
			for (int num_texts = 0; num_texts < title_menu_.texts_.size(); num_texts++)
			{
				title_menu_.texts_[num_texts].Draw();
			}
			SDL_RenderPresent(renderer_);
		}
	}
	return runModeReturnValue;

}
int GameController::CharacterSelect()
{
	SDL_Event events;
	int mouse_x;
	int mouse_y;
	int running_mode_return_value = MAXINT;
	Camera* camera_ptr = Camera::getCamera();
	Character* new_player;
	b2Vec2 spawnpoint;
	

	while (running_mode_return_value > character_menu_.buttons_.size()){
		while(SDL_PollEvent(&events)){

			if(events.type == SDL_MOUSEBUTTONDOWN){
				SDL_GetMouseState(&mouse_x,&mouse_y);
				//mouse_x =camera_ptr->mouse_x_to_screen(mouse_x);
				//mouse_y =camera_ptr->mouse_y_to_screen(mouse_y);
				for(int i = 0; i < character_menu_.buttons_.size(); i++){
					if(character_menu_.buttons_[i].HitTest(mouse_x,mouse_y))
					{
						running_mode_return_value = i;
					};
				};
				break;
			}

			//draw tittlemenu

			/*Updating renderer*/
			SDL_RenderClear(renderer_);
			//Draws the background, nice and easy
			character_menu_.background_->DrawAsBackGround();
			for (int num_buttons = 0; num_buttons < character_menu_.buttons_.size(); num_buttons++)
			{
				character_menu_.buttons_[num_buttons].Draw();
			}
			for (int num_texts = 0; num_texts < character_menu_.texts_.size(); num_texts++)
			{
				character_menu_.texts_[num_texts].Draw();
			}
			SDL_RenderPresent(renderer_);
		}
	}


	return running_mode_return_value;
}
int GameController::IngameMenu() //ingame pause menu
{
	return 0;
}

int GameController::GameOverScreen(const bool you_won){
	bool local_quit = false;
	SDL_Event events;
	int mouse_x;
	int mouse_y;
	SDL_ShowCursor(1);
	camera_ptr_->Menu_Mode(); //sets camera to menumode
	while (!local_quit){
		SDL_RenderClear(renderer_);
		game_over_menu_.backgrounds_[you_won].DrawAsBackGround();
		game_over_menu_.button_->Draw();
		SDL_RenderPresent(renderer_);
		while(SDL_PollEvent(&events)){

			if(events.type == SDL_MOUSEBUTTONDOWN){
				SDL_GetMouseState(&mouse_x,&mouse_y);
				if(game_over_menu_.button_->HitTest(mouse_x,mouse_y))
					local_quit = true;
			}
		}
		
		
	}

	return false;
};

void GameController::load_selected_stage(int stage) {
	switch(stage){
	case 0:
		LoadStage("../Assets/TestLevel.svg");
		break;

	case 1:
		LoadStage("../Assets/NoeTest.svg");
		break;
	}
}

void GameController::load_selected_character(int character) {
	Camera* camera_ptr = Camera::getCamera();
	Character* new_player;
	b2Vec2 spawnpoint;

	spawnpoint = stage_->spawn_point_chars_[rand()%stage_->spawn_point_chars_.size()];
	new_player = new Character(spawnpoint.x, spawnpoint.y, physics_controller_,character,stage_->agents_.size(), factory_);
	camera_ptr->set_character_size(new_player->get_width(),new_player->get_height());
	stage_->agents_.push_back(*new_player);

	number_characters_on_stage_ = stage_->agents_.size(); //sets the active number of players
}

int GameController::OptionsMenu(){
	MainWindow * main_window_instance_ptr = MainWindow::getMainWindow();
	bool local_quit = false;
	SDL_Event events;
	int mouse_x;
	int mouse_y;
	SDL_ShowCursor(1);
	camera_ptr_->Menu_Mode(); //sets camera to menumode
	while (!local_quit){
		SDL_RenderClear(renderer_);
		options_menu_.background_->DrawAsBackGround();
		for(int i  = 0 ; i < options_menu_.buttons_.size() ; i++) {
			options_menu_.buttons_[i].Draw();
		}

		SDL_RenderPresent(renderer_);
		while(SDL_PollEvent(&events)){

			if(events.type == SDL_MOUSEBUTTONDOWN){
				SDL_GetMouseState(&mouse_x,&mouse_y);
				if(options_menu_.buttons_[0].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(640, 480);
					camera_ptr_->set_screen_size(640,480);
				}
				if(options_menu_.buttons_[1].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(800, 600);
					camera_ptr_->set_screen_size(800,600);
				}
				if(options_menu_.buttons_[2].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(1152, 864);
					camera_ptr_->set_screen_size(1152,864);
				}
				if(options_menu_.buttons_[3].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(1280, 700);
					camera_ptr_->set_screen_size(1280,700);
				}
				if(options_menu_.buttons_[4].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(1440, 900);
					camera_ptr_->set_screen_size(1440,900);
				}
				if(options_menu_.buttons_[5].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize(1920, 1080);
					camera_ptr_->set_screen_size(1920,1080);
				}
				
				if(options_menu_.buttons_[6].HitTest(mouse_x,mouse_y)) {
					main_window_instance_ptr->setSize();
				}
				if(options_menu_.buttons_[7].HitTest(mouse_x,mouse_y)) {
					local_quit = true;
				}
			}
		}
		
		
	}

	return 0;

}