#include "Lua.h"

Lua::Lua() {

}

void Lua::Lua_Set_Bot(AI *instance) {
	bot_ = instance;
};

void Lua::Lua_Script(std::string lua_script) {
	lua_script_ = lua_script;
}

void Lua::Lua_Init() {
	L_ = lua_open();
	luaL_openlibs(L_);
	//To add functions from C++ for Lua to use:
	//lua_register(queue_,"lua_function_name",cpp_function_name);
	//Note: Function must be static!

	lua_register(L_,"get_closest_platform",Lua::Get_Closest_Platform);
	lua_register(L_,"get_platform_edge",Lua::Get_Platform_Edge);
	lua_register(L_,"get_target_visible",Lua::Get_Target_Visible);

	int size = luaL_loadfile(L_,lua_script_.c_str());
	if ( size==0 ) {
		// execute Lua program
		size = lua_pcall(L_, 0, LUA_MULTRET, 0);
	}
	if ( size!=0 ) {
		std::cerr << "-- " << lua_tostring(L_, -1) << std::endl;
		lua_pop(L_, 1); // remove error message
	}
}

void Lua::Lua_End() {
	lua_close(L_);
}

void Lua::Move_Right() {

}

int Lua::Get_Closest_Platform(lua_State *L) {
	//bot_->Move("left");
	return 0;	
}
int Lua::Get_Platform_Edge(lua_State *L) {
	return 0;
}
int Lua::Get_Target_Visible(lua_State *L) {
	return 0;
}