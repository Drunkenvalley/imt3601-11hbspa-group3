#include "Bar.h"

Bar::Bar():GraphicsAsset(-50,-50,"../Assets/hpbarBlue.png"){
}

//Constructor that passes the x_pos, y_pos and filename to the GraphicsAsset constructor.
Bar::Bar(int x_pos,int y_pos,string filename):GraphicsAsset(x_pos,y_pos,filename)
{
	image_id_green_ = sprites_->get_sprite_id("../Assets/hpbarGreen.png");
	image_id_red_ = sprites_->get_sprite_id("../Assets/hpbarRed.png");
	image_id_blue_ = sprites_->get_sprite_id("../Assets/hpbarBlue.png");
	image_id_ammo_empty_ = sprites_->get_sprite_id("../Assets/ammoFull.png");
	image_id_ammo_ = sprites_->get_sprite_id("../Assets/ammoEmpty.png");
}

//Destructor
Bar::~Bar(void)
{
}
//Sets the value of the Bar to the number passed, max 300.
void Bar::set_value_ammo(int ammo){
	ammo_ = ammo;
}
void Bar::set_value_hp(int hp){
	value_ = hp;
}

void Bar::set_max_ammo(int new_max_ammo){
	max_ammo_ = new_max_ammo;
}

//Draws the Bar at the position passed.
void Bar::Draw(float x, float y){
	
	if(value_ > 200) {
		image_id_ptr_partial_ = &image_id_blue_;
		image_id_ptr_full_ = &image_id_green_;
	}
	else if(value_ > 100) {
		image_id_ptr_partial_ = &image_id_green_;
		image_id_ptr_full_ = &image_id_red_;
	}else { 
		image_id_ptr_partial_ = &image_id_red_;
		image_id_ptr_full_ = &image_id_;
	}
	//Set the right procentage for each bar
	float procent_to_draw = 1 + ((value_-1)% WIDTH_OF_FULL_HP_BAR); 

	sprites_->DrawSprite(*image_id_ptr_full_,  x, y-WIDTH_OF_FULL_HP_BAR/2);
	sprites_->DrawSprite(*image_id_ptr_partial_,  x, y-WIDTH_OF_FULL_HP_BAR/2,procent_to_draw);
	sprites_->DrawSprite(image_id_ammo_empty_,  x + WIDTH_OF_FULL_HP_BAR/2 , y-WIDTH_OF_FULL_HP_BAR/2);
	sprites_->DrawSpriteVerticalBar_inverted(image_id_ammo_,  x + WIDTH_OF_FULL_HP_BAR/2, y-WIDTH_OF_FULL_HP_BAR/2,float(ammo_ / max_ammo_*100.0f));
}

