#pragma once
#include "GraphicsAsset.h"
class Item :
	public GraphicsAsset
{
private:
	int item_type_;
public:
	//Constructor that takes the x and y position followed by
	// the string to file and lastly the type of item.
	Item(int,int,string,int);
	~Item(void);
	//Checks if you are hitting the Item.
	bool CheckDistance(float,float);
	//Returns the item type.
	int get_item_type(void);
};

