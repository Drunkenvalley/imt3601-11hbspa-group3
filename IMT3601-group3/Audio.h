#pragma once
#include <SDL.h>
#include <string>
#include <iostream>
#include <vector>
#include <SDL_mixer.h>
#include <map>

class Audio
{
private:
	//Vector of sound chunks.
	std::vector<Mix_Chunk*> sound_;
	//Vector of the strings that are passed as names,this vector matches
	// up to the sound chunks vector, so name 0 is chunk 0.
	std::vector<std::string> soundNames_;
	//Vector of music chunks.
	std::vector<Mix_Music*> music_;
	//Music index from the music chunk.
	int music_index_;
	//Private instance pointer and constructor for
	// singelton fun times.
	static Audio* AudioObject_;
	Audio(void);
	//Private functions that sees if a spessified song is loaded,
	// tries to load the othervise. Returns -1 on error, index if success.
	int FindSong(int);
	int FindSong(std::string);
	
public:
	~Audio(void);
	static Audio* getInstance(void);
	//Loads a sound from a string to filepath. True if successfull, false on fail.
	bool LoadSound(std::string);
	//Loads a music file from a filepath, returns true on success.
	bool LoadMusic(std::string);
	//Plays a soundfile with spessified string, true on success.
	bool PlaySound(std::string);
	//Starts playing music.
	void PlayMusic(void);
	//Sets the music volume between 0-255.
	void set_music_volume(int);
	//Sets the sound volume between 0-255.
	void set_sound_volume(int);
	//Plays the next song in the song vector.
	void PlayNextSong(void);
	//Plays the song at index if it excists.
	void PlaySongAtIndex(int);
	//Pauses the music.
	void PauseMusic(void);
	//Resumes the music.
	void ResumeMusic(void);
	//Stops all sound/songs/music from being played.
	void StopSoundPlayback(void);
};
