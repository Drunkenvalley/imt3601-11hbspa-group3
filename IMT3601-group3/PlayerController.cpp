#include "Dictionary.h"
#include "PlayerController.h"
//Constructor takes a string to a config file.
PlayerController::PlayerController(std::string file){
	input_flag_ = 0x0;
	//Loads config
	Config config(file);		
	//Initialize class with keybinds.
	key_jump_ = SDL_GetKeyFromName(config.GetSetting("Keybinds","jump","w").c_str());
	key_left_ = SDL_GetKeyFromName(config.GetSetting("Keybinds","left","a").c_str());
	key_right_ = SDL_GetKeyFromName(config.GetSetting("Keybinds","right","d").c_str());
	key_down_ = SDL_GetKeyFromName(config.GetSetting("Keybinds","down","s").c_str());
	//Should figure out the code to load in special keys.
	key_shoot_ = SDL_BUTTON_LEFT;
	key_quit_ = SDLK_ESCAPE;	
}
//Updates based on a sent SDL Event.
Uint32 PlayerController::Update(SDL_Event action) {
	switch(action.type) {
	case SDL_KEYDOWN:
		     if (action.key.keysym.sym == key_jump_)	{input_flag_ |= JUMP;}
		else if (action.key.keysym.sym == key_left_)	{input_flag_ |= LEFT;}
		else if (action.key.keysym.sym == key_right_)	{input_flag_ |= RIGHT;}
		else if (action.key.keysym.sym == key_down_)	{input_flag_ |= DOWN;}
		break;
	case SDL_KEYUP:
		     if (action.key.keysym.sym == key_jump_)	{input_flag_ |= JUMP SHIFT;}
		else if (action.key.keysym.sym == key_left_)	{input_flag_ |= LEFT SHIFT;}
		else if (action.key.keysym.sym == key_right_)	{input_flag_ |= RIGHT SHIFT;}
		else if (action.key.keysym.sym == key_down_)	{input_flag_ |= DOWN SHIFT;}
		break;
	case SDL_MOUSEBUTTONDOWN:
		if (action.button.button == key_shoot_)	{input_flag_ |= SHOOT;}
		break;
	case SDL_MOUSEBUTTONUP:
		if (action.button.button == key_shoot_)	{input_flag_ |= SHOOT SHIFT;}
		break;
	}
	return input_flag_;
}
//Sets the input flags to the sent Uint32.
void PlayerController::set_input_flags_(Uint32 input) {
	input_flag_ = input;
}

Uint32 PlayerController::get_input_flags(void) {
	return input_flag_;
}