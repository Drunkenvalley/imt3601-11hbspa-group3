#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <queue>

#include "Dictionary.h"
#include "sdl.h"
#include "SDL_net.h"
#include "Character.h"
#include "ShotFactory.h"
#include "PhysicsController.h"
#include "World.h"


const Uint16 default_client_port_ = 8881; // set to 0 for a random port
const Uint16 default_server_port_ = 8880;

const int packet_size_ = 512; //constant int size of packets
class Network
{
private:
	static Network* network_instance_;
	bool is_host_;
	int stage_id_;
	int characters_;
	int character_id_;
	int character_type_;
	stage_objects* stage_; 
	ShotFactory* shot_factory_;
	PhysicsController* physics_controller_;
	Uint8 critical_packet_id_;

	UDPsocket socket_;        /* Socket descriptor */
	UDPpacket *packet_;       /* Pointer to packet memory */

	Uint16 client_port_;  

	IPaddress server_address_;		/* extern servers address, port to use and more*/
	char * server_name_;				// just the IP Adress of "server".
	Uint16 server_port_;

	struct ClientConnection;
	std::vector<ClientConnection> connection_list_;
	std::vector<UDPpacket> critical_packets_;

public:
	Network(Uint16 client_port, char *server_name, Uint16 server_port, bool isHost);
	~Network(void);
	Uint64 get_packet_input(void);
	void send_packet(Uint64 input);
	bool get_is_server(void);
	void get_packet(void);
	void send_packet(void);
	void create_packet(int character_id, Uint64 input, double rotation);
	void send_packets(int character_id, Uint64 input, double rotation);
	void send_character_positions(int character_id, int pos_x, int pos_y, double rotation);
	void send_bullet_positions(int bullet_id, int pos_x, int pos_y);
	void client_handshake(Uint8 character_type);
	void server_handshake(IPaddress client, int character_id, int map_id);
	void client_close_connection();
	void server_close_connection(int character_id);
	void get_input_packet(Uint32* input, double* angle);
	void send_input_packet(Uint32 input, double angle);
	int get_character_id();
	int get_stage();
	int get_opponent();
	int get_characters();
	void client_update();
	void client_status_check();
	void set_externals(stage_objects* stage, ShotFactory* factory, PhysicsController* physics_controller);
	void set_stage_id(int selected_stage);
	void send_update(int character_type);
	bool add_client(IPaddress ip);
	void send_critical_packets();
	void verify_packet(IPaddress ip, int id);
	void send_update_packets();
	static Network* get_instance();
	static Network* get_instance(Uint16 client_port, char *server_name, Uint16 server_port, bool isHost);

	struct character_movement_packet{
		int character_id_;
		Uint32 input_;
		double rotation_;
	};

	struct server_update_packet{
		int character_id_;
		int character_type_;
		int action_;			// create/delete
	};

	struct ClientConnection{
		IPaddress address;
		Uint32 ticks_last_message_;
	};
};
