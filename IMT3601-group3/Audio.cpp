#include "Audio.h"

Audio* Audio::AudioObject_ = NULL;

Audio::Audio(void){
	music_index_ = NULL;
	//Sets up the SDL Audio
	if( Mix_OpenAudio( MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) { 
		printf("Error in opening and starting audio!\n"); 
	}
}

Audio::~Audio(void){
	//Frees up all sound chunks
	for (int i = 0; i < music_.size(); i++)
	{
		Mix_FreeMusic(AudioObject_->music_[i]);
	}

	for (int i = 0; i < AudioObject_->sound_.size(); i++){
		Mix_FreeChunk(AudioObject_->sound_[i]);
	}
	//Empties the vectors
	AudioObject_->soundNames_.clear();
	AudioObject_->sound_.clear();
	AudioObject_->music_.clear();
	//Quit SDL_mixer
	Mix_CloseAudio();
	Mix_Quit();
}

/*
Returns a pointer to the audio object.
*/
Audio* Audio::getInstance(){
#pragma omp critical		
	if( AudioObject_ == nullptr ){
			{
			AudioObject_ = new Audio;
			}
		}
	return AudioObject_;
}

void Audio::PlayMusic(){
	//If there is music loaded and it is not playing
	if(AudioObject_->music_.empty() == 0){
		if(Mix_PlayMusic( AudioObject_->music_[music_index_], -1)){
			printf("Couldn't play music at index %d.\n", AudioObject_->music_index_);
		}
	}else{
		printf("No music loaded!\n");
	}
}
//Plays a song with the passed string as a identifier. String is the filepath to the sound.
// Returns true if playing the sound was possible.
bool Audio::PlaySound(std::string sound){
	int index;

	//Tries to find the index of the song, or load if not loaded
	index = AudioObject_->FindSong(sound);

	if( index == -1){
		return 0;
	}
	//Plays the sound in the first availible channel with 0 loops
	//Index is -1 since I do index++ in the end of my while loop
	if( Mix_PlayChannel(-1, AudioObject_->sound_[index], 0) == -1){
		printf("Error in playing sound! Look in PlaySound!\n");
		return false;
	}
	return true;
}
//Loads music from the passed string to the filepath.
//Returns true if success, false on fail.
bool Audio::LoadMusic(std::string filepath){
	Mix_Music* load;
	//LoadMUS supports WAVE, MOD, MIDI, OGG, MP3, FLAC
	load = Mix_LoadMUS(filepath.c_str());
	if( load == NULL ){
		printf("Error in loading music: %s\n", filepath.c_str());
		return false;
	}
	AudioObject_->music_.push_back(load);
	return true;
}
//Loads a sound from the passed string to the filepath.
//Returns true if success, false on fail.
bool Audio::LoadSound(std::string filepath){
	Mix_Chunk *load;
	//LoadWAV supports WAVE, AIFF, RIFF, OGG, and VOC
	load = Mix_LoadWAV( filepath.c_str() );
	if (load == NULL ){
		printf("Error in loading sound: %s\n", filepath.c_str());
		return false;
	}
	AudioObject_->sound_.push_back(load);
	AudioObject_->soundNames_.push_back(filepath);
	return true;
}

//Setts the volume for the music
void Audio::set_music_volume(int volume){
	Mix_VolumeMusic(volume);
}

//Setts the volume for all channels
void Audio::set_sound_volume(int volume){
	Mix_Volume(-1, volume);
}

//Plays the next song in the vector.
void Audio::PlayNextSong(){

	static int fadeTimer_ = 50;
	//Fades out current music if there is one playing
	if(Mix_PlayingMusic == 0){
		Mix_FadeOutMusic(fadeTimer_);
	}
	//Increments the index counter
	AudioObject_->music_index_++;
	AudioObject_->music_index_ = AudioObject_->music_index_ % AudioObject_->music_.size();

	//Fades in the new music
	Mix_FadeInMusic(music_[music_index_], -1, fadeTimer_);
}

//Plays a song at selected index
void Audio::PlaySongAtIndex(int index){
	static int fadeTimer_ = 50;

	//Forrandret til �Ev�re ikke -1 etter Alvins forrandringer
	if( AudioObject_->FindSong(index) != -1){
		if(Mix_PlayingMusic() == 1){
			Mix_FadeOutMusic(fadeTimer_);
		}
		Mix_FadeInMusic(music_[index], -1, fadeTimer_);
		Mix_PlayMusic(music_[index], -1);
		AudioObject_->music_index_ = index;
	}
}

//Pauses the currently playing music
void Audio::PauseMusic(){
	Mix_PauseMusic();
}

//Resumes the music, safe to use even if music is playing
void Audio::ResumeMusic(){
	Mix_ResumeMusic();
}

//Pauses playback for all channels.
void Audio::StopSoundPlayback(){
	Mix_PauseMusic();
	Mix_Pause(-1);
}

int Audio::FindSong(std::string name){
	//Searches through the vector of strings to find the sound index
	//Returns index if found
	if (AudioObject_->soundNames_.empty())
	{
		LoadSound(name);
	}
	for (int index = 0; index < AudioObject_->soundNames_.size(); index++){
		if (name == AudioObject_->soundNames_[index]){
			return index;
		};
	}

	//If the file is not loaded we attempt to load it
	//Return the sound index if found 
	if (AudioObject_->LoadSound(name)){
		return AudioObject_->soundNames_.size()-1;
	};

	//The name was not found or loaded
	printf("Error in playing file: %s", name);
	return -1;
}
int Audio::FindSong(int index){
	if( index > (AudioObject_->music_.size() ) && AudioObject_->music_.empty() != 1){
		return index;
	}else{
		return -1;
	}
}
