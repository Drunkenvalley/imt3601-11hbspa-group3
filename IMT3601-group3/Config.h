#pragma once
#include <string>
#include <Windows.h>
#include "Dictionary.h"

using namespace std;

class Config {
private:
	string file_;
public:
	//Constructor that takes a string to the path of the config file.
	Config(string);
	//Returns the string to a setting if it excists, take a string to the section
	// a string to the option you want, and a string as a default return value.
	string GetSetting(string section,string option,string default);
	//Returns a int to the setting, takes a string for the section it is under,
	// a string to the option and a int as the default return value.
	int GetSetting(string section,string option,int default);
	//Returns a bool to the setting, takes a string to the section it is under
	// and a string to the the option it is under.
	bool GetSetting(string section,string option);


	//Writes a string to the .ini file under the chosen section & setting
	void SetSetting(string section, string option, string output);
};