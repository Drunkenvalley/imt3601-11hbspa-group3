#include "Camera.h"
#include <omp.h>
#include <SDL.h>
//Null object for the singelton instance.
Camera* Camera::camera_instance_ = NULL;
//Empty constructor.
Camera::Camera(void)
{}

Camera::~Camera(void)
{}
//Singelton principial return of the object.
//Conseptualy threadsafe.
Camera* Camera::getCamera()
{
#pragma omp critical
	{
		if (camera_instance_ == NULL)
		{
			camera_instance_ = new Camera();
		}
	}
	return camera_instance_;
}
//Centers the camera to the middle of the screen. Used for menus.
void Camera::Menu_Mode(){
	x_ = -screen_width_/2;
	y_ = -screen_height_/2;
};
//Sets the camera to follow the dot passed. Keeps the camera within bounds of the world.
void Camera::set_pos(float x, float y){
	//Center the camera over the dot
	x_ = ( x + player_width_ / 2 ) - screen_height_ / 2;
	y_ = ( y + player_height_ / 2 ) - screen_width_ / 2;

	//Keep the camera in bounds.
	if( x_ < 0 ){
		x_ = 0;    
	}
	if( y_ < 0 ){
		y_ = 0;    
	}
	if( x_ > world_width_ - screen_width_ ){
		x_ = world_width_ - screen_width_;    
	}
	if( y_ > world_height_ - screen_height_ ){
		y_ = world_height_ - screen_height_;    
	}
}
//Sets the size of the character.
void Camera::set_character_size(int width,int height)
{
	player_width_ = width;
	player_height_ = height;
};
//Sets the size of the world.
void Camera::set_world_size(int width,int height)
{
	world_width_ = width;
	world_height_ = height;
};
//Sets the size of the screen.
void Camera::set_screen_size(int width,int height)
{
	screen_width_ = width;
	screen_height_ = height;
};
//Returns the offsett between screen and camera in x.
float Camera::get_x()
{
	return x_;
};
//Returns the offsett between screen and camera in y.
float Camera::get_y()
{
	return y_;
}
//Returns the height of the screeen.
int Camera::get_screen_height() 
{
	return screen_height_;
};
//Returns the width of the screen.
int Camera::get_screen_width()
{
	return screen_width_;
}
//Converts the passed x to world coordinates from screen.
int Camera::mouse_x_to_screen(int x){ 
	// x = 0 -> 799
	return x+x_ ;
};
//Converts the passed y to world coordinates from screen.
int Camera::mouse_y_to_screen(int y){
	// y = 0 -> 599
	return y+y_ ;
};