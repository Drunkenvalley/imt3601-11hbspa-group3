#include "ShotFactory.h"

#define DEAD_ZONE -50.0f,-50.0f
#define DEFAULT_AMMO 100

//Constructor that takes a pointer to the Physics Controller.
ShotFactory::ShotFactory(PhysicsController* physicControl)
{
	ShotFactory(physicControl, DEFAULT_AMMO);
}
//Constructor that takes a pointer to the Physics Controller and the
// maximum amount of bullets needed in the game.
ShotFactory::ShotFactory(PhysicsController* physicControl, int max_bullet)
{
	//Sets the physics controller.
	physics_controller_ = physicControl;
	//Resets the bullet itterator and sets the maximum amount of bullets.
	bullet_ittr_ = 0;
	vector_size_ = max_bullet;
	//Resizes the vector of Shot's for more efficient making of Shot's.
	bullet_vector_.resize(max_bullet);

	//Defines the shape of the Box2D body bullet.
	bullet_shape_.m_radius = 0.05f;
	bullet_shape_.SetAsBox(prototype_shot_.get_img_height()/2/BOX_2D_SCALING,
		prototype_shot_.get_img_width()/2/BOX_2D_SCALING);
	//Defines the fixture of the bullet.
	bullet_fixture_def_.shape = &bullet_shape_;
	bullet_fixture_def_.density = (BULLET_MASS);
	bullet_fixture_def_.friction = (BULLET_FRICTION);
	//SURPRISE MOTHA FUCKA!
	bullet_fixture_def_.filter.groupIndex = -8;
	//Body definitions of the bullet.
	bullet_body_def_.type = b2_dynamicBody;
	bullet_body_def_.position.Set(DEAD_ZONE);
	bullet_body_def_.fixedRotation = false;
	bullet_body_def_.bullet = true;
	//Fills the vector with Shot that is ready to be shot.
	for (int i = 0; i < max_bullet; i++)
	{
		bullet_vector_.push_back(Shot(-50,-50,AMMO_NAME_ONE));

		b2Body* new_bullet;
		new_bullet = physics_controller_->AddBody(&bullet_body_def_);
		new_bullet->CreateFixture(&bullet_fixture_def_);
		new_bullet->SetSleepingAllowed(true);
		new_bullet->SetActive(false);
		new_bullet->SetAwake(false);
		//Gives the new bullet a unique ID.
		new_bullet->GetFixtureList()[0].SetUserData((void*) (i + FIXTURE_OFFSET_TO_SHOTS));
		bullet_vector_[i].set_body(new_bullet);
		
	}
	//Resizes the vector back after inserts to save some memory.
	bullet_vector_.resize(max_bullet);
}

ShotFactory::~ShotFactory(void)
{
}
//Returns the pointer of a Shot that is outside the boundries of the world.
Shot* ShotFactory::nextShot(void)
{
	return nextShot(DEAD_ZONE);
}

Shot* ShotFactory::nextShot(int x_pos, int y_pos)
{
	bullet_ittr_++;
	if (bullet_ittr_ >= vector_size_)
	{
		bullet_ittr_ = 0;
	}
	//Sets the Shots body to be active.
	bullet_vector_[bullet_ittr_].Activate();
	//Moves the body to the passed coordinates.
	bullet_vector_[bullet_ittr_].set_body_pos(x_pos, y_pos);
	return &bullet_vector_[bullet_ittr_];
}
//Function for shooting a bullet with a different image index
Shot* ShotFactory::nextShot(int x_pos, int y_pos, int image_index)
{
	Shot* new_bullet = nextShot(x_pos,y_pos);
	new_bullet->set_image_id(image_index);
	new_bullet->set_weapon(true);
	return new_bullet;
}
//Function for returning a Shot back to the factory when it is not needed.
void ShotFactory::returnBullet(Shot* oldBullet)
{
	
	//Resets and deactivates a normal bullet
	for (int i = 0; i < bullet_vector_.size(); i++)
	{
		if (&bullet_vector_[i] == oldBullet)
		{
			bullet_vector_[i].reset_image_id();
			bullet_vector_[i].set_weapon(false);
			bullet_vector_[i].set_body_pos(DEAD_ZONE);
			//Sets the velocity to nothing.
			bullet_vector_[i].set_velocity(b2Vec2_zero);
			bullet_vector_[i].Deactivate();
			return;
		}
	}
}