#pragma once
#include "graphicsasset.h"
#include "Button.h"
#include "Sprites.h"
class Slider
{
private:
	//Rectange for drawing the slider.
	SDL_Rect slider_rect_;
	//Colour of the slider.
	SDL_Colour slider_colour_;
	//Pointer to a button.
	Button* button_draggable_;
	//Pointer to sprite image.
	GraphicsAsset* sprite_image_;
	//Current value along the axis.
	float current_value_;
	//Maximum value.
	int max_value_;
	//Mouse pointer offset along the x axis so it does not snap to center.	
	int mouse_pointer_offset;
public:
	//Constructor with a top left point, max value, start value and string to image file
	Slider(SDL_Point,int,int, std::string);
	//Constructor that takes a rectangle, maximum value, start value, colour of slider and colour of the box.
	Slider(SDL_Rect, int, int, SDL_Colour, SDL_Colour);
	~Slider(void);
	void Draw(void);
	//Moves the box along the movements of the mouse.
	void MoveBox(void);
};