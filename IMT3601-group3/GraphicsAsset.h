#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <string>
#include "Sprites.h"

using namespace std;

class GraphicsAsset
{
protected:
	Sprites* sprites_;
	//Rotation angle of the GraphicsAsset.
	double rotation_;
	//Current position in the world.
	float pos_x_;
	float pos_y_;
	//Information about the Sprite image of the GraphicsAsset.
	int image_id_;
	int image_width_;
	int image_height_;
	//Animation data for the GraphicsAsset.
	unsigned int row_count_;
	unsigned int col_count_;
	unsigned int frame_count_;
	//Flip flags for rendering.
	bool flip_horizontal;
	bool flip_vertical;
	SDL_RendererFlip flip_flags_;
public:
	//Constructor that takes the top left x and y postition of the image and 
	// the string to the image.
	GraphicsAsset(int,int,string);
	~GraphicsAsset(void);
	//Returns the index of the GraphicsAsset.
	int get_img_index(void);
	//Returns the width of the GraphicsAsset.
	int get_img_width(void);
	//Returns the height of the GraphicsAsset.
	int get_img_height(void);
	//Returns the x postion of the GraphicsAsset.
	float get_pos_x(void);
	//Returns the y position of the GraphicsAsset.
	float get_pos_y(void);
	//Moves the image to a new position following the top left corner.
	void set_pos(float,float);
	//set a different image // NB! make sure the image is loaded;
	void set_image_id(int);
	//Sets the rotation of the image.
	void set_rotation(double);
	//Adds a double degree of rotation to the set rotation.
	void add_rotation(double);
	//Sets the horizontal flag for flipping the sprite.
	void set_flip_horizontal(bool);
	//Sets the vertical flag for flipping the sprite.
	void set_flip_vertical(bool);
	//Flips the current state of the vertical flip flag.
	void FlipVertical(void);
	//Flips the current state of the horizontal flip falg.
	void FlipHorizontal(void);
	//Sets the animation data; number of rows, number of collums, number of frames
	// the height of each image, and lastly the width of each image.
	void set_animation_data(unsigned int,unsigned int,unsigned int,unsigned int,unsigned int);
	void DrawAsBackGround();
	virtual void Draw(void);
	virtual void Draw(float x_pos, float y_pos);
	void Draw(float pos_x, float pos_y, float rotation);
};

