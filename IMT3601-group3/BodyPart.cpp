#include "BodyPart.h"
//Constructor that passes the x_pos, y_pos and string to filename to GraphicsAsset constructor.
BodyPart::BodyPart(int x_pos,int y_pos,string filename):GraphicsAsset(x_pos,y_pos,filename)
{
}

BodyPart::~BodyPart(void)
{
}
//Drawing function that renders the sprite in the active renderer.
void BodyPart::Draw(){
	sprites_->DrawSprite(image_id_,pos_x_,pos_y_,rotation_);
}