#include <SDL.h>
#include <lua.hpp> 
#include <string>
#include <Box2D\Box2D.h>
#include "Dictionary.h"
#include "Utils.h"
#include "World.h"
#include "AI.h"

class Lua {
	//static AI *bot_;
	AI *bot_;

	std::string lua_script_;
	lua_State *L_;

public:
	Lua();
	~Lua();

	void Lua_Set_Bot(AI *instance);
	void Lua_Script(std::string lua_script);
	void Lua_Init();
	void Lua_End();

	void Move_Right();
	void Move_Left();
	void Move_Jump();
	void Move_Shoot();

	static int Get_Closest_Platform(lua_State *L);
	static int Get_Platform_Edge(lua_State *L);
	static int Get_Target_Visible(lua_State *L);
};
