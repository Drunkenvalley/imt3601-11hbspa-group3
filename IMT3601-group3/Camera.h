#pragma once

class Camera
{
private:
	//Positions.
	float x_;
	float y_;
	//Size of the world.
	int world_width_;
	int world_height_;
	//Screen sizes.
	int screen_width_;
	int screen_height_;
	//Size of the player, used to move camera around.
	int player_width_;
	int player_height_;
	//Singelton construct.
	Camera(void);
	~Camera(void);
	static Camera* camera_instance_;
public:
	static Camera* getCamera(void);
	//Return the camera upper left x.
	float get_x(void);
	//Returns the camera upper left y.
	float get_y(void);
	//Get functions for screen sizes.
	int get_screen_height(void);
	int get_screen_width(void);
	//Returns the mouse position in the world from the screen x.
	int mouse_x_to_screen(int);
	//Returns the mouse position in the world from the screen y.
	int mouse_y_to_screen(int);
	//Sets the camera to be in the senter of the screen, used for menues.
	void Menu_Mode(void);
	//Sets the position of the camera in world space to center on plater.
	void set_pos(float,float);
	//Set width and height of the character.
	void set_character_size(int,int);
	//Sets the width and height of the world.
	void set_world_size(int,int);
	//Sets the width and height of the screen.
	void set_screen_size(int,int);
};

