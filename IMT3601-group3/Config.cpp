#include "Config.h"

Config::Config(string file) {
	file_ = file;
}

bool Config::GetSetting(string section,string option) {
	/* This requires you call a 'section' to search for 'option' in;
	 * A section in config.ini is denoted as [Section], like [Window]
	 * Unless the value of your option is 1, we assume it's false. */
	if(GetPrivateProfileInt(section.c_str(),option.c_str(),0,file_.c_str()) == 1) {
		return true;
	}
	else return false;
}

int Config::GetSetting(string section,string option,int default) {
	/* This requires you call a 'section' to search for 'option' in;
	 * A section in config.ini is denoted as [Section], like [Window]
	 * This function requires a default value, which is returned if the option is undefined 
	 * or is not an int value. */
	return GetPrivateProfileInt(section.c_str(),option.c_str(),default,file_.c_str());
}

string Config::GetSetting(string section,string option,string default) {
	char result[1024];
	
	GetPrivateProfileString(section.c_str(),option.c_str(),default.c_str(),result,1024,file_.c_str());
	string output(result);

	//printf("resultat: %s",result);

	return output;
}

void Config::SetSetting(string section, string option, string output) {
	WritePrivateProfileString(section.c_str(),option.c_str(), output.c_str(),file_.c_str());
}