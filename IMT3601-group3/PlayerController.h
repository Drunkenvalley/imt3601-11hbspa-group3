#pragma once
#include <SDL.h>
#include "Config.h"

class PlayerController
{
private:
	//32 bits of input flag, 16 for saying something happened,
	// 16 bits for saying something stopped.
	Uint32 input_flag_;
	int key_jump_;
	int key_left_;
	int key_right_;
	int key_down_;
	int key_quit_;
	int key_shoot_;

public:
	//Constructor with a string to a config file
	PlayerController(std::string);
	//Updates the inputflags with the SDL Event and returns the new input flags.
	Uint32 Update(SDL_Event action);
	Uint32 get_input_flags(void);
	void set_input_flags_(Uint32 input);
};
