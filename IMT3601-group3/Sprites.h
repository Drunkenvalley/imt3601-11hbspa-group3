#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <vector>
#include "Camera.h"
#include "Dictionary.h"

class Sprites
{
	std::vector<SDL_Texture*> images_;
	std::vector<std::string> image_names_;
	static Sprites *sprite_object_; 
	//Pointer to the current SDL renderer object
	SDL_Renderer* renderer_;
	//Constructors, all private
	Sprites(void);
	Sprites(Sprites const&);
	void operator=(Sprites const&);
	//Rectangles used in rendering
	SDL_Rect source_rectangle_;
	SDL_Rect destination_rectangle_;

public:
	~Sprites(void);
	static Sprites* getSpritesInstance();
	//Returns the name of the spriteID.
	std::string get_name(int id);
	int get_sprite_id(std::string kName);
	int get_width(int kIndex);
	int get_height(int kIndex);
	bool LoadImage(std::string filepath);
	//Draw images fixed in window.
	bool DrawStationary(int kIndex, float x, float y); 
	// draw stretched over the whole screen
	bool DrawAsBackground(int kIndex);

	//Draw images.
	bool DrawSprite(int kIndex, float x, float y); 
	//Draw images with scaling in the x dirrection.
	bool DrawSprite(int kIndex, float x, float y, float x_scale); 
	//Draw images with scaling in the y dirrection.
	bool DrawSpriteVerticalBar_inverted(int kIndex, float x, float y,float y_scale);

	//Draw images with rotation.
	bool DrawSprite(int kIndex, float x, float y,double rotation); 
	//Draw images with rotation with the center not being the center of the sprite
	bool DrawSprite(int kIndex, float x, float y, double rotation, SDL_Point center);
	//Draw images with a rotation and/or flipped axes.
	bool DrawSprite(int kIndex, float x, float y, double rotation,SDL_RendererFlip flip_flags);
	//Draw images with rotation not being the center of the sprite and/or flipped axises.
	bool DrawSprite(int kIndex, float x, float y, double rotation, SDL_Point center, SDL_RendererFlip flip_flags);
	//Sets the renderer, called if you change the SDL renderer.
	void setRenderer();
};