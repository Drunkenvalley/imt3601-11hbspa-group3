#pragma once
#include "graphicsasset.h"
class Button :
	public GraphicsAsset
{
private:
	//No private variables here.
public:
	//Takes the x and y position and a string to the filepath/name.
	Button(int,int,string);
	~Button(void);
	//Hit detection to see if you hit the button with the mouse,
	// takes the mouse x and mouse y position.
	bool HitTest(int,int);
	//Sets the x position of the button, takes a int to the new position.
	void set_x_position(int);
	//Sets the y position of the button, takes a int to the new position.
	void set_y_position(int);
	//Draws the Button in the renderer.
	void Draw();

};

