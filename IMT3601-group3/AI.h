#pragma once
#include <SDL.h>	//Because Uint32 is apparently from SDL.
#include <lua.hpp> 
#include <string>
#include <Box2D\Box2D.h>
#include "Dictionary.h"
#include "Utils.h"
#include "World.h"

//Classrelated structs
struct relative_player {
	bool left;
	bool right;
	bool above;
	bool below;
	bool visible;
	int distance;
	int standing_on;
};
struct relative_platforms {
	int standing_on;
	vector<int> left;
	vector<int> right;
	vector<int> above;
	vector<int> below;
};
struct current_platform {
	int left;
	int right;
};

class AI
{
	//Stage
	stage_objects* stage_;

	//Relative positions to bot (Left, right, above and/or below)
	relative_player player_position_;
	relative_platforms platforms_position_;
	current_platform standing_on_;

	double rotation_;

public:
	AI(void);
	~AI(void);

	//Basic functionality
	void Move(std::string direction);

	void FindPlatformOrientation(void);
	void FindPlayerOrientation(void);
	void FindPlayerDistance(void);

	void FindLeftEdge(int i);
	void FindRightEdge(int i);

	bool Visible(void);
	double WeaponAngle(void);

	Uint32 Actions(void);
};

