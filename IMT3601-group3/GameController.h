#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <Box2D/Box2D.h>
#include <vector>
#include <iostream>
#include "PlayerController.h"
#include "Camera.h"
#include <fstream>
#include "Audio.h"
#include "World.h"
#include "AI.h"
//gui
#include "Button.h"
#include "Bar.h"
#include "Slider.h"
#include "Text.h"
#include "Network.h"

#include "MainWindow.h"

using namespace std;

class GameController
{
private:
	static GameController* gamecontroller_instance_;
	//Constructor has to be private.
	GameController();

	int number_characters_on_stage_;// numebr of active players
	AI bot_;
	Sprites* list_of_sprites_; //could be set up in advance but will now insted be set up when the
	//constructors of graphicsassets are run.
	SDL_Renderer* renderer_;	//this is the main renderer for everything in the window
	//other classes will have pointers to the same renderer
	PlayerController *player_input_;
	// File for replay output
	FILE *file_ptr_;
	//Audio pointer
	Audio* audio_;
	//World pointer
	World* world_;
	// Stage info pointer
	stage_objects* stage_;
	//Camera pointer
	Camera* camera_ptr_;
	//Physics controller	
	PhysicsController* physics_controller_;
	//World wide factory for bullets
	ShotFactory* factory_;
	//Struct for having a simple menu.
	struct SimpleMenu {
		GraphicsAsset* background_;
		vector<Button> buttons_;
		vector<Text> texts_;
	};
	struct GameOverMenu {
		vector<GraphicsAsset> backgrounds_;
		Button* button_;

	};
	//Struct for the Options menu.
	struct OptionsMenu{
		GraphicsAsset* background_;
		vector<Button> buttons_;
		vector<Text> texts_;
		vector<Slider> sliders_;
	};
	//Title menu for the game.
	SimpleMenu title_menu_;
	//Options menu for the game.
	OptionsMenu options_menu_;
	//Pause menu for the game.
	SimpleMenu ingame_menu_; 
	//Character select menu.
	SimpleMenu character_menu_; 
	//Stage select menu.
	SimpleMenu stage_menu_;
	//Function for writing all events to a log file.
	GameOverMenu game_over_menu_;
	void WriteLog(void);
	//Network controller.
	Network* network_controller_;
public:
	~GameController(void);
	static GameController* getGameController();
	//Renders the game without physics updates.
	int PausedRender();
	//Event handler for the game, takes a reference to the SDL Event.
	bool HandleEvents(SDL_Event&);
	//Loads a game stage, takes a string to the file path.
	bool LoadStage(const string);
	void load_selected_stage(int stage);
	void load_selected_character(int character);
	//Unloads the stage, returns true if success.
	bool UnLoadStage(void);
	//Returns a pointer to the stage objects struct.
	stage_objects* GetStage(void);
	//Sets the input flags to a player.
	void set_input(Uint32,int);
	//Sets the log file that all events are written to.
	void set_logfile(string);
	//Handles what happens when a character is hit by a bullet. returns -1 if no victor, 0 if local wins, 1 if another wins.
	int HandleCharacterHitByBullet(void);

	/*
	*ALL THESE METHODS ARE DEFINED IN GameControllerMenus.cpp
	*/
	//TItle menu for the game, first screen.
	int TitleMenu(void);
	//Stage select screen.
	int StageSelect(void);
	//Character select screen.
	int CharacterSelect(void);
	//Pause menu.
	int IngameMenu(void);
	// options menu for changing resolution
	int OptionsMenu();
	//Running game, this updates and renders the game.
	int GameStage(void);
	//Decides what happens when you press a button.
	int TitleMenuButtonActions(int id);
	//Shows if you win or lose, button to return
	int GameOverScreen(const bool you_won);
	//Get network packets
	void GetNetworkPackets();
	void send_network_updates();
};
