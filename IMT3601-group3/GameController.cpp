
#include "GameController.h"
#include "Renderer.h"
#include "Dictionary.h"
#include <iostream>
#include <fstream>
#include <time.h>

GameController* GameController::gamecontroller_instance_ = NULL;

GameController* GameController::getGameController()
{
#pragma omp critical
	{
		if (gamecontroller_instance_ == NULL)
		{		
			gamecontroller_instance_ = new GameController();	
		}
	}

	return gamecontroller_instance_;
}

GameController::GameController()
{
	Renderer* renderer = Renderer::getRenderer();
	renderer_ = renderer->get_renderer_object();
	world_ = World::get_world();
	list_of_sprites_ = Sprites::getSpritesInstance();

	player_input_ = new PlayerController(CONFIG_FILE);

	audio_ = Audio::getInstance();
	audio_->LoadMusic(BG_MUSIC);
	audio_->PlayMusic();
	audio_->set_music_volume(50);
	//audio_->set_sound_volume(0);

	//Camera pointr
	camera_ptr_ = Camera::getCamera();

	//init Title Menu

	//init Ingame Menu
	title_menu_.background_ = new GraphicsAsset(0,0,"../assets/greenframe.png");
	Button button(400,300,"../assets/TitleLocalButton.png"); //is drawen with the camera as origo
	title_menu_.buttons_.push_back(button);
	button = Button(400,350,"../assets/TitleNetworkButton.png");
	title_menu_.buttons_.push_back(button);
	button = Button(400,400,"../assets/optionsButton.png");
	title_menu_.buttons_.push_back(button);
	button = Button(400,450,"../assets/TitleQuitButton.png");
	title_menu_.buttons_.push_back(button);

	//init Character Select
	character_menu_.background_ = new GraphicsAsset(0,0,"../assets/charselectbackground.png");
	button = Button(400,300,"../assets/SelectCharacter1Button.png");
	character_menu_.buttons_.push_back(button);
	button = Button(400,350,"../assets/SelectCharacter2Button.png");
	character_menu_.buttons_.push_back(button);
	button = Button(400,400,"../assets/SelectCharacter3Button.png");
	character_menu_.buttons_.push_back(button);
	//init Stage Select
	stage_menu_.background_ = new GraphicsAsset(0,0,"../assets/stageselectbackground.png");
	button = Button(400,300,"../assets/SelectStage1Button.png");
	stage_menu_.buttons_.push_back(button);
	button = Button(400,350,"../assets/SelectStage2Button.png");
	stage_menu_.buttons_.push_back(button);
	//init Options Menu

	//init gameover menu
	game_over_menu_.button_ = new Button(400,400,"../Assets/awesomeButton.png");

	GraphicsAsset background(0,0,"../assets/YOU_WON.png"); //is drawen with the camera as origo
	game_over_menu_.backgrounds_.push_back(background);
	background = GraphicsAsset(0,0,"../assets/YOU_LOSE.png");
	game_over_menu_.backgrounds_.push_back(background);

	//init gameover menu
	options_menu_.background_ = new GraphicsAsset(0,0,"../assets/backgroundOptions.png");
	button = Button(400,200,"../assets/rez640x480.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,250,"../assets/rez800x600.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,300,"../assets/rez1152x864.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,350,"../assets/rez1280x700.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,400,"../assets/rez1440x900.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,450,"../assets/rez1920x1080.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,500,"../assets/fullscreenButton.png");
	options_menu_.buttons_.push_back(button);
	button = Button(400,550,"../assets/returnButton.png");
	options_menu_.buttons_.push_back(button);
}

GameController::~GameController(void)
{
	fclose(file_ptr_);
}

int GameController::GameStage() //running game
{
	/*Updating renderer*/
	SDL_RenderClear(renderer_);

	//Draws the background, nice and easy
	stage_->background_->Draw();

	//Loops through all the platforms and draws them in order.
	for (int ant_platforms = 0; ant_platforms < stage_->plattforms_.size(); ant_platforms++)
	{
		//stage_->plattforms_[ant_platforms].Draw();
	}


	//Loops through the list of agents in a stage and draws them.
	for (int ant_agents = 0; ant_agents < stage_->agents_.size(); ant_agents++)
	{
		//Here you will draw the agents when there is a draw function for it
		stage_->agents_[ant_agents].draw();

	}

	if(network_controller_ == NULL || network_controller_->get_is_server()) {
		//Loops through all the pickup items that is on the stage.
		for (int ant_pickups = 0; ant_pickups < stage_->pickups_.size(); ant_pickups++) {
			stage_->pickups_[ant_pickups].Draw();
		}
		for (int ant_bullets = 0; ant_bullets < stage_->bullets_.size(); ant_bullets++) {
			if (stage_->bullets_[ant_bullets]->IsMoving() && stage_->bullets_[ant_bullets]->get_alive_time() <= BULLET_TIMEOUT) {
				stage_->bullets_[ant_bullets]->Draw();

			} else {
				factory_->returnBullet(stage_->bullets_[ant_bullets]);
				//stage_->bullets_[ant_bullets].destroyBullet();
				stage_->bullets_.erase(stage_->bullets_.begin()+ant_bullets);
				ant_bullets--;
			}
		}
	} else {
		for (int ant_bullets = 0; ant_bullets < stage_->bullets_.size(); ant_bullets++) {
				stage_->bullets_[ant_bullets]->Draw();
		}
		for (int ant_pickups = 0; ant_pickups < stage_->pickups_.size(); ant_pickups++) {
			stage_->pickups_[ant_pickups].Draw();
		}
	}
	SDL_RenderPresent(renderer_);

	return 1;
}
int GameController::PausedRender() // render the gamestage without physics or movement
{
	return 0;
}



bool GameController::LoadStage(const string filepath)
{

	if(!world_->LoadStage(filepath)){
		return false;
	}
	stage_ = world_->get_stage();
	auto stage_info = stage_->background_->get_img_index();
	int WorldWidth = list_of_sprites_->get_width(stage_info);
	int WorldHeight = list_of_sprites_->get_height(stage_info);
	Camera* camera_ptr = Camera::getCamera();
	camera_ptr->set_world_size(WorldWidth,WorldHeight);
	physics_controller_ = new PhysicsController(WorldWidth,WorldHeight);
	factory_ = new ShotFactory(physics_controller_, 200);
	for (int i = 0, num_platforms = stage_->plattforms_.size(); i < num_platforms; i++)
	{
		stage_->plattforms_[i].set_b2body(physics_controller_->CreatePlatform(
			stage_->plattforms_[i].get_pos_x(), 
			stage_->plattforms_[i].get_pos_y(), 
			stage_->plattforms_[i].get_width(), 
			stage_->plattforms_[i].get_height()
			));
		//printf("platform %d (%d %d %d %d)\n", i, platforms_[i].get_pos_x(), platforms_[i].get_pos_y(), platforms_[i].get_width(), platforms_[i].get_height());
	}
	for (b2Vec2 item : stage_->spawn_point_items_){
		stage_->pickups_positions_.push_back(false);
	}

	return true;
}

void GameController::set_input(Uint32 input_flags,int player_number){
	stage_->agents_[player_number].InputUpdate(input_flags);
}

bool GameController::HandleEvents(SDL_Event &events){
	//player_input_->set_input_flags_(0x0);
	while(SDL_PollEvent(&events)){
		//If we are quitting we return it as false.

		switch (events.type)
		{
		case SDL_KEYDOWN:
			player_input_->Update(events);break;
		case SDL_KEYUP:
			//Quit on escape key 
			if (events.key.keysym.sym == SDLK_ESCAPE)
			{
				return false;
			}
			player_input_->Update(events);break;
		case SDL_MOUSEBUTTONDOWN:
			player_input_->Update(events);break;
		case SDL_MOUSEBUTTONUP:
			player_input_->Update(events);break;
		case SDL_USEREVENT:
			stage_->agents_[events.user.code].InputUpdate((Uint32)events.user.data1,*(double*) events.user.data2); break;
		default:
			break;
		};
	}
	//::::::::::::: UPDATES THE PLAYERS INPUT :::::::::::::::::::::::::::
	stage_->agents_[LOCAL_PLAYER_NUMBER].InputUpdate(player_input_->get_input_flags());
	if (network_controller_ == NULL) {
		stage_->agents_[LOCAL_BOT_NUMBER].InputUpdate(bot_.Actions(),bot_.WeaponAngle());

	} else {
		network_controller_->send_critical_packets();
		//network_controller_->client_status_check();
		if(network_controller_->get_is_server()) {

			network_controller_->client_update();
			for (int i = 0; i < stage_->bullets_.size(); i++){
				if (stage_->bullets_[i]->IsMoving()) {
					network_controller_->send_bullet_positions(i, stage_->bullets_[i]->get_pos_x(), stage_->bullets_[i]->get_pos_y());
				}
			}
			
		} else {
			network_controller_->send_input_packet(player_input_->get_input_flags(), stage_->agents_[LOCAL_PLAYER_NUMBER].get_angle()); 
		}
	}

	if(network_controller_ == NULL || network_controller_->get_is_server()){
		for (int ant_agents = 0; ant_agents < stage_->agents_.size(); ant_agents++)
		{
			//Pushes the bullets into a vector
			Shot* shot_ptr;
			shot_ptr = stage_->agents_[ant_agents].FireBullet();
			if (shot_ptr)
			{
				stage_->bullets_.push_back(shot_ptr);
			}

		}
	}

	//::::::::::::::::Update the Worldstate::::::::::::::::::::::::::

	//Loops through the list of agents in a stage and draws them.
	if (network_controller_->get_is_server()) {
		for (int ant_agents = 0; ant_agents < stage_->agents_.size(); ant_agents++) {
			//Here you will draw the agents when there is a draw function for it
			stage_->agents_[ant_agents].Update(&stage_->pickups_);
		}
	} else {
		for (int ant_agents = 0; ant_agents < stage_->agents_.size(); ant_agents++) {
			//Here you will draw the agents when there is a draw function for it
			stage_->agents_[ant_agents].Update();
		}
	}
	//make new items
	while (stage_->pickups_.size() < stage_->spawn_point_items_.size()){
		int i = rand() % stage_->spawn_point_items_.size();
		if(!stage_->pickups_positions_[i])
			stage_->pickups_.push_back(Item(stage_->spawn_point_items_[i].x,stage_->spawn_point_items_[i].y,"../Assets/Pickup.png",rand() % NUMBER_OF_WEAPONS));
	}




	//Update camera
	Camera* camera_ptr = Camera::getCamera();
	camera_ptr->set_pos(stage_->agents_[LOCAL_PLAYER_NUMBER].get_x_pos(), stage_->agents_[LOCAL_PLAYER_NUMBER].get_y_pos());

	//Write events to log file
	WriteLog();
	
	
	if(network_controller_ == NULL || network_controller_->get_is_server()) {
		physics_controller_->updateWorld();
		if(network_controller_ != NULL && network_controller_->get_is_server()) {
			send_network_updates(); // !!! 
		}
	}
	//Restest the input flags in the PlayerController
	//player_input_->set_input_flags_(0x0);

	int end_state = HandleCharacterHitByBullet(); // see if the battle is over
	if(end_state >= 0){
		GameOverScreen(end_state);
		return false;
	}

	if(network_controller_ == NULL || network_controller_->get_is_server()){
	for (int ant_bullets= 0; ant_bullets < stage_->bullets_.size(); ant_bullets++) {
			stage_->bullets_[ant_bullets]->Update();
		}
	}

	player_input_->set_input_flags_(0x0);
	return true;
}



/* Clears all vectors from the stage to make it loadable. */
bool GameController::UnLoadStage(void)
{
	world_->UnloadStage();
	//delete physics_controller_;
	delete factory_;
	return true;
}

stage_objects* GameController::GetStage() {
	return stage_;
};

void GameController::set_logfile(string FileName){
	//Resets the rand and saves the seed to the log file
	Uint32 rand_seed = time(NULL);
	srand(rand_seed);

	//Opens a file to write replay if it can be opened.
	if (fopen_s(&file_ptr_,FileName.c_str(),"w") == NULL)
	{
		fprintf_s(file_ptr_,"Random seed is: %u\n",rand_seed);
		fprintf_s(file_ptr_,"Structure is: Frame Player Input\n");
		fprintf_s(file_ptr_,"Stage Path: %s\n", stage_->stage_file_.c_str());
	}
}
void GameController::WriteLog(void)
{
	//If there is a file to print the replay to
	if (file_ptr_)
	{
		Uint32 currentTick = SDL_GetTicks();
		Uint32 input_flags;
		for (Uint16 current_player = 0; current_player < stage_->agents_.size(); current_player++)
		{
			Character someone = stage_->agents_.at(current_player);
			input_flags = someone.get_input_flags();
			//No reason to ouput if there is no input
			if (input_flags)
			{
				fprintf(file_ptr_,"%u %u %u\n",currentTick, current_player, input_flags);
			}
		}
	}
}


int GameController::HandleCharacterHitByBullet(){
	vector<CharHitEvent> che_events;
	physics_controller_->GetCharHitEvents(che_events);
	for(CharHitEvent evt : che_events){

		if(!stage_->agents_[evt.char_id_].UpdateHp(20)){
			number_characters_on_stage_--;
			if(number_characters_on_stage_ < 2){
				if(stage_->agents_[LOCAL_PLAYER_NUMBER].is_active()){
					return 0;
				} else return 1;
			}
		}
		
		for(int i = 0; i < stage_->bullets_.size(); i++){

			if(evt.bullet_id_ == stage_->bullets_[i]->get_btd_id()){
				factory_->returnBullet(stage_->bullets_[i]);
				//stage_->bullets_[ant_bullets].destroyBullet();
				//stage_->bullets_.erase(stage_->bullets_.begin() + i);
				//i--; //because one item has been removed and we don't want to skip the next one
			}
		}


	}
		che_events.clear();

	return -1;
}

//Get network packets
void GameController::GetNetworkPackets() {
	network_controller_->get_packet();
}

void GameController::send_network_updates() {
	for (int i = 0; i < stage_->agents_.size(); i++) {
		network_controller_->send_character_positions(i, stage_->agents_[i].get_x_pos(), 
														stage_->agents_[i].get_y_pos(), 
														stage_->agents_[i].get_angle());
	}
}																							// !!! send updates
