#pragma once
#include "graphicsasset.h"
class BodyPart :
	public GraphicsAsset
{
private:
	//Private variable for the rotation of the body part.
	double rotation_;
public:
	//Constructor that takes a top left x, top left y and a string to sprite image.
	BodyPart(int,int,string);
	~BodyPart(void);
	//Draws the body part in the active renderer.
	void Draw(void);
};

