#pragma once
#include <cmath>
#include "graphicsasset.h"
#include "Shot.h"
#include "Dictionary.h"
#include "ShotFactory.h"

class Weapon :
	public GraphicsAsset
{
private:
	//Pointer to the factory from the game world
	ShotFactory* bullet_factory_;
	//Vector of bullets that the weapon have shot
	std::vector<Shot> bullets_;
	string file_name_of_ammo;
	//SDL Point to the senter of the weapon
	SDL_Point center_;
	//Angle that the weapon is rotated to
	double angle_;
	//Amount of ammo and its type
	int ammo_;
	int ammo_type_;


	int weapon_lenght_;
	int arm_lenght_;
public:
	Weapon(string, int, int, int, int, ShotFactory*);
	~Weapon(void);
	double get_angle(void);
	Shot* FireBullet(void);
	void Update(int x,int y);
	void UpdatePos(int x,int y);
	double get_radians(void);
	void set_angle(double angle);
	void Draw(void);
	void ReturnBullet(Shot*);
	int get_ammo();
};
