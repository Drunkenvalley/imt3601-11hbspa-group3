#include "Scenery.h"
#include "Sprites.h"

//Constructor takes the top left position and a string to the image file.
Scenery::Scenery(int x_pos,int y_pos, string file_name):GraphicsAsset(x_pos,y_pos,file_name)
{
}
//Constructor takes the top left position followed by the width and height 
// and lastly a string to the image file.
Scenery::Scenery(int pos_x,int pos_y, int width, int height):GraphicsAsset(pos_x,pos_y,"../Assets/ArrowBox.png")
{
	pos_x_ = pos_x;
	pos_y_ = pos_y;
	width_ = width;
	height_ = height;
}

Scenery::~Scenery(void)
{
}
//Draws the Scenery in the current Renderer.
void Scenery::Draw()
{
	sprites_->DrawSprite(image_id_, pos_x_, pos_y_);
}

int Scenery::get_pos_x() {
	return pos_x_;
}

int Scenery::get_pos_y() {
	return pos_y_;
}

int Scenery::get_width() {
	return width_;
}

int Scenery::get_height() {
	return height_;
}
//Sets the Box2D body
void Scenery::set_b2body(b2Body* body) {
	platform_body_ = body;
}

b2Body* Scenery::get_b2body() {
	return platform_body_;
}