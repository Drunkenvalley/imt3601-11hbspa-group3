#pragma once
#include "shot.h"
#include <Box2D\Box2D.h>
#include "PhysicsController.h"
class ShotFactory
{
private:
	vector<Shot> bullet_vector_;
	Shot prototype_shot_;
	int vector_size_;
	int bullet_ittr_;
	PhysicsController* physics_controller_;
	b2PolygonShape bullet_shape_;
	b2FixtureDef bullet_fixture_def_;
	b2BodyDef bullet_body_def_;

public:
	ShotFactory(PhysicsController*);
	ShotFactory(PhysicsController*, int);
	~ShotFactory(void);
	Shot* nextShot(void);
	Shot* nextShot(int,int);
	Shot* nextShot(int,int,int);
	void returnBullet(Shot*);
};

