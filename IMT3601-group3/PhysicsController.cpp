#include "PhysicsController.h"
//Constructor takes the width and height of the world.
PhysicsController::PhysicsController(int width, int height)
{
	//The Box2D world definitions
	btdGravity = b2Vec2(0.0f,9.8f);
	btdWorld = new b2World(btdGravity);
	//Box2D world body definitions.
	b2BodyDef worldEdgeBoxDef; 
	worldEdgeBoxDef.position.Set(0, 0);
	//Adds the edges of the world to the world.
	b2Body* worldEdgeBoxBody = btdWorld->CreateBody(&worldEdgeBoxDef);
	//Generates the world.
	generate_world_box(worldEdgeBoxBody, width, height);
	//Box2D Settings.
	velocityIterations = 6;
	positionIterations = 2;
	contact_listener_ = new BtdContactListener;
	btdWorld->SetContactListener(contact_listener_); 
	//Timing variables.
	thisTime_ = SDL_GetTicks();
	lastTime_ = SDL_GetTicks();
}
//Updates the phyiscs to the current timeframe.
void PhysicsController::updateWorld()
{
	thisTime_ = SDL_GetTicks();
	deltaTime_ = (thisTime_ - lastTime_)/1000.0f;
	lastTime_ = SDL_GetTicks();
	//Updates the world with a delta timestep.
	btdWorld->Step(deltaTime_, velocityIterations, positionIterations);
	
}
//Adds a Box2D body definition to the physics world.
b2Body* PhysicsController::AddBody(b2BodyDef* bodyDef)
{
	return btdWorld->CreateBody(bodyDef);
}
//Generates the world box.
void PhysicsController::generate_world_box(b2Body* worldBox, int width, int height)
{
	b2PolygonShape edgeBox;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &edgeBox;
	//Top.
	CreateBox(worldBox, -BOX_2D_SAFETYBOARDER_THICKNESS, -BOX_2D_SAFETYBOARDER_THICKNESS, width/BOX_2D_SCALING+BOX_2D_SAFETYBOARDER_THICKNESS*2, BOX_2D_SAFETYBOARDER_THICKNESS); 
	//Left.
	CreateBox(worldBox, -BOX_2D_SAFETYBOARDER_THICKNESS, -BOX_2D_SAFETYBOARDER_THICKNESS, BOX_2D_SAFETYBOARDER_THICKNESS, height/BOX_2D_SCALING+BOX_2D_SAFETYBOARDER_THICKNESS*2);
	//Right.
	CreateBox(worldBox, width/BOX_2D_SCALING, -BOX_2D_SAFETYBOARDER_THICKNESS, BOX_2D_SAFETYBOARDER_THICKNESS, height/BOX_2D_SCALING+BOX_2D_SAFETYBOARDER_THICKNESS*2);
	//Buttom.
	CreateBox(worldBox, -BOX_2D_SAFETYBOARDER_THICKNESS, height/BOX_2D_SCALING, width/BOX_2D_SCALING+BOX_2D_SAFETYBOARDER_THICKNESS*2, BOX_2D_SAFETYBOARDER_THICKNESS);
}
//Creaes a platform out of the the coordinates sent.
b2Body* PhysicsController::CreatePlatform(int pos_x1, int pos_y1, int pos_x2, int pos_y2)
{
	//Body definitions.
	b2BodyDef worldPlatformDef; 
	worldPlatformDef.position.Set(0, 0);
	b2Body* worldPlatformBody = btdWorld->CreateBody(&worldPlatformDef);

	CreateBox(worldPlatformBody, float(pos_x1/BOX_2D_SCALING), float(pos_y1/BOX_2D_SCALING), 
		float((pos_x2-pos_x1)/BOX_2D_SCALING), float((pos_y2-pos_y1)/BOX_2D_SCALING));
	//Returns a pointer to the newly created platform.
	return worldPlatformBody;
}
//Creates a box out of the coordinates sent.
void PhysicsController::CreateBox(b2Body* box, float pos_x, float pos_y, float width, float height)
{
	//Body definitions.
	b2PolygonShape fixture;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &fixture;
	fixture.SetAsBox(width/2, height/2, b2Vec2(pos_x + width/2, pos_y + height/2), 0);
	box->CreateFixture(&fixtureDef);
}
//Fills sent vector with all collision events since last update.
void PhysicsController::GetCharHitEvents(std::vector<CharHitEvent> & s){
	return contact_listener_->GetCharHitEvents(s);
	
};
//Gets the number of foot collisions since the last update.
int PhysicsController::get_foot_sensor_collisions(int index) {
	return contact_listener_->get_foot_sensor_collisions(index);
}
