#include "AI.h"
using namespace std;

#define STAGE		stage_->
#define PLAYER		stage_->agents_[LOCAL_PLAYER_NUMBER]
#define BOT			stage_->agents_[LOCAL_BOT_NUMBER]
#define PLATFORMS	stage_->plattforms_

AI::AI() {
	World* tempWorld = World::get_world();
	stage_ = tempWorld->get_stage();

	player_position_.above = false;
	player_position_.below = false;
	player_position_.left = false;
	player_position_.right = false;
	player_position_.visible = false;
	player_position_.distance = 0;
}

AI::~AI() {

}

void AI::Move(string direction) {
	if(direction.c_str()=="jump") {New_Event(LOCAL_BOT_NUMBER,(void*) JUMP,0);}
	else if(direction.c_str()=="left") {New_Event(LOCAL_BOT_NUMBER,(void*) LEFT,0);}
	else if(direction.c_str()=="right") {New_Event(LOCAL_BOT_NUMBER,(void*) RIGHT,0);}
}

void AI::FindPlatformOrientation() {
	//Clearing the vectors.
	platforms_position_.left.clear();
	platforms_position_.right.clear();
	platforms_position_.above.clear();
	platforms_position_.below.clear();
	//Don't want to extend these forever with duplicates.

	for(int i = 0; i < PLATFORMS.size(); i++) {
		if(BOT.get_y_pos()+BOT.get_height()==PLATFORMS[i].get_pos_y()) {
			platforms_position_.standing_on = i;
			break;
		}
		if(PLAYER.get_y_pos()+PLAYER.get_height()==PLATFORMS[i].get_pos_y()) {
			player_position_.standing_on = i;
			break;
		}
	}

	for(int i = 0; i < PLATFORMS.size(); i++) {
		if(i == platforms_position_.standing_on) {
			continue;
		}

		//Check if platform is to the right of bot. Add to vector if true.
		if(BOT.get_x_pos()+BOT.get_width() <= PLATFORMS[i].get_pos_x()) {
			platforms_position_.right.push_back(i);
		}

		//Check if platform is to the left of bot. Add to vector if true.
		if(BOT.get_x_pos() >= PLATFORMS[i].get_pos_x()+PLATFORMS[i].get_img_width()) {
			platforms_position_.left.push_back(i);
		}

		//Check if platform is below the bot. Add to vector if true.
		if(BOT.get_y_pos()+BOT.get_height() <= PLATFORMS[i].get_pos_y()) {
			platforms_position_.below.push_back(i);
		}

		//Check if platform is above the bot. Add to vector if true.
		if(BOT.get_y_pos() >= PLATFORMS[i].get_pos_y()+PLATFORMS[i].get_img_height()) {
			platforms_position_.above.push_back(i);
		}
	}
};
void AI::FindPlayerOrientation() {
	for (int i = 0; i < PLATFORMS.size(); i++)
	{
		if (BOT.get_x_pos()+BOT.get_width() <= PLAYER.get_x_pos())
		{
			//WHAT TO DO IF THE PLAYER IS TO THE RIGHT OF THE BOT
			player_position_.right = true;
		}
		else {
			player_position_.right = false;
		}
		if (BOT.get_x_pos() >= PLAYER.get_x_pos() + PLAYER.get_width())
		{
			//WHAT TO DO IF THE PLAYER IS TO THE LEFT OF THE BOT
			player_position_.left = true;
		}
		else {
			player_position_.left = false;
		}
		if (BOT.get_y_pos()+BOT.get_height() <= PLAYER.get_y_pos())
		{
			//WHAT TO DO IF THE PLAYER IS BELOW THE BOT
			player_position_.below = true;
		}
		else {
			player_position_.below = false;
		}
		if (BOT.get_y_pos() >= PLAYER.get_y_pos() + PLAYER.get_height())
		{
			//WHAT TO DO IF THE PLAYER IS ABOVE THE BOT
			player_position_.above = true;
		}
		else {
			player_position_.above = false;
		}
	}
}

void AI::FindPlayerDistance() {
	if(PLAYER.get_x_pos() < BOT.get_x_pos()) {
		player_position_.distance = BOT.get_x_pos() - PLAYER.get_x_pos();
	}
	else {
		player_position_.distance = PLAYER.get_x_pos() - BOT.get_x_pos();
	}
}

void AI::FindLeftEdge(int i) {
	standing_on_.left =
		PLAYER.get_x_pos()
		-PLATFORMS[i].get_pos_x()
		;

}
void AI::FindRightEdge(int i) {
	standing_on_.left =
		PLATFORMS[i].get_pos_x()
		+PLATFORMS[i].get_width()
		-PLAYER.get_x_pos()
		-PLAYER.get_width()
		;
};

bool AI::Visible() {
	b2RayCastInput input;

	input.p1.x = BOT.get_x_pos() + (BOT.get_width()/2);
	input.p1.y = BOT.get_y_pos() + (BOT.get_height()/2);
	input.p2.x = PLAYER.get_x_pos() + (PLAYER.get_width()/2);
	input.p2.y = PLAYER.get_y_pos() + (PLAYER.get_height()/2);
	input.maxFraction = 1;

	float closestFraction = 1;

	for (int i = 0; i < PLATFORMS.size(); i++) {
		b2Body* b = PLATFORMS[i].get_b2body();
		for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
			b2RayCastOutput output;

			if ( ! f->RayCast( &output, input, NULL ) )
				continue;
			if ( output.fraction < closestFraction ) {
				closestFraction = output.fraction;
			}
		}
	}

	if(closestFraction == 1) {
		return true;
	}
	else {
		return false;
	}
}

double AI::WeaponAngle() {
	int x = BOT.get_x_pos();
	int y = BOT.get_y_pos();
	int pos_x = PLAYER.get_x_pos(); 
	int pos_y = PLAYER.get_y_pos();

	//Angle calculation between mouse and character.
	pos_x = x - pos_x;
	pos_y = y - pos_y;
	rotation_ = -atan2(pos_x,pos_y) * 180 / M_PI-90;
	return rotation_;
}

Uint32 AI::Actions() {
	Uint32 input_flag = 0x0;

	FindPlayerOrientation();
	FindPlayerDistance();

	if(player_position_.distance > 200) {
		if(player_position_.left) {
			input_flag |= LEFT;
		}
		else {
			input_flag |= LEFT SHIFT;
		}
		if(player_position_.right) {
			input_flag |= RIGHT;
		}
		else {
			input_flag |= RIGHT SHIFT;
		}
		if(player_position_.above) {
			input_flag |= JUMP;

		}
		else {
			input_flag |= JUMP SHIFT;
		}
	}
	else {
		if(player_position_.left) {
			input_flag |= RIGHT;
		}
		else {
			input_flag |= RIGHT SHIFT;
		}
		if(player_position_.right) {
			input_flag |= LEFT;
		}
		else {
			input_flag |= LEFT SHIFT;
		}
		if(player_position_.below) {
			input_flag |= JUMP;
		}
		else {
			input_flag |= JUMP SHIFT;
		}
	}

	if(Visible()) {
		input_flag |= SHOOT;
	}

	return input_flag;
}