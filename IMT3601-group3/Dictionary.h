#pragma once

/*This file exists exclusively to create a common list of typedefs and defines*/

/*CONFIG FILE*/
#define CONFIG_FILE "../Include/config.ini"	//This links to the location of the config.ini file.

#define DEFAULT_SCREEN_WIDTH 800.0
#define DEFAULT_SCREEN_HEIGHT 600.0

/*PLAYER INPUT
 *We read input, then define an int with what actions we registered. Then other functions
 *can simply check the value to find out what is being done. This was done because we
 *wanted all input to be able to share a method of manipulation.
 *
 *Defining the value globally means that we don't have to remember what values
 *we're checking.*/
#define JUMP 0x8	//Input was user jumping
#define LEFT 0x4	//User goes left
#define RIGHT 0x1	//User goes right
#define DOWN 0x2	//User goes right
#define SHOOT 0x10	//User presses shootin button

#define SHIFT << 16	//Shifting 16 bits right

#define SLIGHT_DECREASE 0.9

#define LOCAL_PLAYER_NUMBER 0
#define LOCAL_BOT_NUMBER 1
#define NETWORK_PLAYER_1_NUMBER 2

/* Global definitions for Box2D*/
#define BOX_2D_SCALING 100.0	// pixels in a meter
#define  BOX_2D_SAFETYBOARDER_THICKNESS 5 

/* Definition for current log file */
#define LOG_FILE "../Assets/CurrentLog1.txt"

/* Definition for music*/
#define BG_MUSIC "../Assets/Resurrection.mp3"
#define JUMP_SOUND "../Assets/dupsinglevocalbounce.wav"

/* Definition of ammo names*/
#define AMMO_NAME_ONE "../Assets/Teset.png"

#define NUMBER_OF_WEAPONS 3
#define NUMBER_OF_PLAYERS 4
#define MAX_NUMBER_OF_SHOTS_PER_CHARACTER 100

#define FIXTURE_OFFSET_TO_SHOTS NUMBER_OF_PLAYERS + NUMBER_OF_PLAYERS +1
//foot sensors have id's 0 to NUMBER_OF_PLAYERS-1
//characters have id's NUMBER_OF_PLAYERS to FIXTURE_OFFSET_TO_SHOTS-1
//shots have id's FIXTURE_OFFSET_TO_SHOTS to (FIXTURE_OFFSET_TO_SHOTS+NUMBER_OF_PLAYERS*MAX_NUMBER_OF_SHOTS_PER_CHARACTER)-1

#define WIDTH_OF_FULL_HP_BAR 100


#define BULLET_TIMEOUT 3000
#define BULLET_MASS 500
#define BULLET_FRICTION 1.0f

//this is struct needed by gamecontroller, btdcontactlistener and phyciscscontroller;
struct CharHitEvent
	{
		int char_id_;
		int bullet_id_;
	};



//network "enum"
#define SERVER_HANDSHAKE_PACKET 0
#define CLIENT_HANDSHAKE_PACKET 1
#define PLAYER_INPUT_PACKET 2
#define CRITICAL_VERIFICATION_PACKET 3
#define CHARACTER_POSITION_PACKET 4
#define BULLET_POSITION_PACKET 5
#define CLIENT_CLOSE_CONNECTION_PACKET 6
#define SERVER_CLOSE_CONNECTION_PACKET 7
#define PICKUP_POSITION_PACKET 8
#define CLIENT_UPDATE_PACKET 9
#define UPDATE_REQUEST_PACKET 10