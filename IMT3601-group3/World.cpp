#include "World.h"
//NULLed instance pointer for lazy instanciation of the singelton.
World* World::world_instance_ = NULL;
//Empty default constructor.
World::World(void)
{
	stage_.stage_loaded_flag = false;
}

World::~World(void)
{
}
//Returns a singelton pointer to the world.
World*  World::get_world(){
#pragma omp critical
	{
		if (world_instance_ == NULL)
		{
			world_instance_ = new World();
		}
	}
	return world_instance_;
}
//Returns a pointer to the stage objects struct.
stage_objects* World::get_stage(){
	return &stage_;
}
//Function for loading a stage into the world. Take a string to stage file.
//File is a modified SVG. Returns true on success, false othervise.
bool World::LoadStage(string filename){
	//Unloads a stage if one is loaded already.
	if (stage_.stage_loaded_flag == true)
	{
		UnloadStage();
	}

	string line_;
	ifstream file_ (filename.c_str());

	if( file_.is_open() != 1){
		printf("Unable to open: %s\n", filename.c_str());
		return false;
	}
	//Saves the name of the file for replay and logging purposes
	this->stage_.stage_file_ = filename;

	getline(file_, line_);
	//Make sure that the first line is the picture path
	if( line_.front() == '"' && line_.back() == '"'){
		//Loads the background into a Grahics Asset object
		line_.erase(0,1);
		line_.pop_back();
		this->stage_.background_ = new GraphicsAsset(0,0,line_.c_str());
	}
	//Index_pos_ is used for finding the first part
	//Index_pos_after is used for finding the ending of the values
	//Index_len_ is used for calculating the length in the substring
	int index_pos_ = 0;
	int index_pos_after_ = 0;
	int index_len_ = 0;
	int string_value_;

	while( getline(file_, line_) ){
		//Remove tabulator from the beggning of lines
		while( line_.find('\t') == 0){
			line_.erase(line_.begin());
		}

		//This will get the size of the stage
		if (line_.find("<svg") == 0){
			//Get the possition for the width of the image
			index_pos_ = line_.find("width=\"") + 7;
			index_pos_after_ = line_.find("px\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;
			//Converts the string into a int
			string_value_ = atoi(line_.substr(index_pos_,index_len_).c_str());
			//Push back instead of printing it
			//Should be saved somewhere reasonable.

			//Get the position for reading the height of the image
			index_pos_ = line_.find("height=\"", index_pos_after_) + 8;
			index_pos_after_ = line_.find("px\"", index_pos_);
			index_len_ = index_pos_after_ = index_len_;
			//Convert the string into a int
			string_value_ = atoi(line_.substr(index_pos_,index_len_).c_str());
			//Push it back instead of printing it.
			//Should be saved somewhere reasonable.
		}
		//Loads in rects for platforms
		else if (line_.find("<rect") == 0 ){
			//Vector to store the values that is being read
			vector<int> value_;
			value_.clear();

			//Get the first x
			index_pos_ = line_.find("x=\"") +3;//Gets the pos for first digit
			index_pos_after_ = line_.find("\"", index_pos_);//Gets the pos for the last digit
			index_len_ = index_pos_after_ - index_pos_;

			//Pushes back the string of numbers as a integer number into the int vector
			value_.push_back( atoi((line_.substr(index_pos_, index_len_)).c_str()));

			//Get the first y
			index_pos_ = line_.find("y=\"", index_pos_after_) +3;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;

			//Pushes back the string of numbers as a integer number into the int vector
			value_.push_back( atoi((line_.substr(index_pos_, index_len_)).c_str()));

			//Get the second x
			index_pos_ = line_.find("width=\"", index_pos_after_) +7;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;

			//Pushes back the new x + the first y to get a composite number of them both
			string_value_ = atoi((line_.substr(index_pos_, index_len_)).c_str()) + value_[0];
			value_.push_back( atoi((line_.substr(index_pos_, index_len_)).c_str()) + value_[0]);
			//Get the second y
			index_pos_ = line_.find("height=\"", index_pos_after_) +8;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;

			//Pushes back the new y + the first y to get a composite number of them both
			value_.push_back( atoi((line_.substr(index_pos_, index_len_)).c_str()) + value_[1]);
			//Creates a new platform.
			Scenery platform(value_[0],value_[1],value_[2],value_[3]);
			//Pushes the new platform into the stage vector.
			stage_.plattforms_.push_back(platform);
			//Empty the vector for further use
			value_.clear();
		}
		//Loads in points for item spawn points
		else if (line_.find("<ellipse") == 0) {

			int value_x_;
			int value_y_;
			//Find the x pos
			index_pos_ = line_.find("cx=\"") +4;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;
			value_x_ = atoi((line_.substr(index_pos_, index_len_)).c_str());
			//Find the y pos
			index_pos_ = line_.find("cy=\"")+4;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;
			value_y_ = atoi((line_.substr(index_pos_, index_len_)).c_str());

			//Push the values for x and y into spawn point for items vector
			stage_.spawn_point_items_.push_back( b2Vec2(value_x_, value_y_) );
		}
		//Loads in the points for character spawn points
		else if (line_.find("<circle") == 0){ 
			int center_x_;
			int center_y_;
			//Find the x pos
			index_pos_ = line_.find("cx=\"") +4;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;
			center_x_ = atoi((line_.substr(index_pos_, index_len_)).c_str());
			//Find the y pos
			index_pos_ = line_.find("cy=\"") +4;
			index_pos_after_ = line_.find("\"", index_pos_);
			index_len_ = index_pos_after_ - index_pos_;
			center_y_ = atoi((line_.substr(index_pos_, index_len_)).c_str());
			//Push the values for x and y into spawn point for chars vector
			stage_.spawn_point_chars_.push_back( b2Vec2(center_x_, center_y_));
		}
	}
	//Sets the position of the stage to be center of the world.
	Sprites *list_of_sprites_ = Sprites::getSpritesInstance();
	int width = list_of_sprites_->get_width(stage_.background_->get_img_index());
	int height = list_of_sprites_->get_height(stage_.background_->get_img_index());
	printf("%d  %d\n",width,height);
	stage_.background_->set_pos(width/2,height/2);
	//Sets the stage loaded flag and returns.
	stage_.stage_loaded_flag = true;
	return true;
}
//Unloads the stage to clear it for refilling and loadng of a new stage.
void World::UnloadStage() {
	stage_.agents_.clear();
	stage_.pickups_.clear();
	stage_.plattforms_.clear();
	stage_.spawn_point_chars_.clear();
	stage_.spawn_point_items_.clear();
	stage_.pickups_positions_.clear();
	stage_.stage_file_.clear();
	stage_.bullets_.clear();
	stage_.stage_loaded_flag = false;
}
//Returns wether the stage is loaded or not.
bool World::StageIsLoaded() {
	return stage_.stage_loaded_flag;
}
