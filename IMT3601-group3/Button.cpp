#include "Button.h"
//Constructor that passes all values along to GraphicsAsset.
Button::Button(int x_pos,int y_pos, string file_name):GraphicsAsset(x_pos,y_pos,file_name)
{
}


Button::~Button(void)
{
}
//Simple hit detection between mouse and this Button.
bool Button::HitTest(int mouse_x, int mouse_y){
	Camera* camera_ptr = Camera::getCamera();
	double scaleRezolutionX = camera_ptr->get_screen_width()/DEFAULT_SCREEN_WIDTH;
	double scaleRezolutionY = camera_ptr->get_screen_height()/DEFAULT_SCREEN_HEIGHT;
	
	int x1_position = pos_x_ - image_width_/2;
	int y1_position = pos_y_ - image_height_/2;
	int x2_position = pos_x_ + image_width_/2;
	int y2_position = pos_y_ + image_height_/2;

	x1_position *= scaleRezolutionX;
	y1_position *= scaleRezolutionY;
	x2_position *= scaleRezolutionX;
	y2_position *=scaleRezolutionY;

	return ( bool( mouse_x >= x1_position && mouse_y >= y1_position && mouse_x < x2_position && mouse_y < y2_position));

}
//Sets the x position.
void Button::set_x_position(int new_x){
	pos_x_ = new_x;
}
//Sets the y position.
void Button::set_y_position(int new_y){
	pos_y_ = new_y;
}
//Draws the Button in the current SDL renderer.
void Button::Draw(){
	sprites_->DrawStationary(image_id_,pos_x_,pos_y_);
}