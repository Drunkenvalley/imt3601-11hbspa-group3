#include "Slider.h"
#define default_width 500
#define default_height 20

//Constructor for a slider that makes a Graphics Asset to use as texture
Slider::Slider(SDL_Point top_left ,int max_value, int current_value, std::string file_name)
{
	//Makes a default sized slider with starting points in the coordinates that are sent to the constructor.
	this->slider_rect_.x = top_left.x;
	this->slider_rect_.y = top_left.y;
	this->slider_rect_.w = default_width;
	this->slider_rect_.h = default_height;
	this->max_value_ = max_value;
	//Setts the current value to max if the current value is greater than the max value.
	if( current_value > max_value) {
		this->current_value_ = max_value;
	} else{
		this->current_value_ = current_value;
	}
	this->sprite_image_ = new GraphicsAsset(top_left.x, top_left.y, file_name);
	//Sets the colour of the slider to nothing.
	this->slider_colour_.a = NULL;
	this->slider_colour_.r = NULL;
	this->slider_colour_.g = NULL;
	this->slider_colour_.b = NULL;
	//Creates a button that can be dragged.
	//this->button_draggable_ = new Button(SDL_COLOUR);

}

/*Constructor that takes colours*/
Slider::Slider(SDL_Rect slider_rect, int slider_max_value, 
			   int slider_start_value, SDL_Colour slider_colour, SDL_Colour box_colour)
{
	this->slider_rect_ = slider_rect;
	//Sets the start value to max if the value is greater than the max value
	if( slider_start_value > slider_max_value) {
		this->current_value_ = slider_max_value;
	} else{
		this->current_value_ = slider_start_value;
	}
	max_value_ = slider_max_value;
	slider_colour_ = slider_colour;
	//Setts the pointer to sprite to null
	sprite_image_ = nullptr;
	//Creates a button that can be dragged on the slider
	//button_draggable_ = new Button(box_colour);
}

Slider::~Slider(void)
{
	delete this->button_draggable_;
	delete this->sprite_image_;
}
//Draws the Slider in the current SDL Renderer.
void Slider::Draw()
{
	if (this->sprite_image_){
		sprite_image_->Draw();
	}else
	{
		printf("No image! Blitting square");
		//SDL_FillRect(Renderer->getInstance(),this->slider_rect_, this->slider_colour_);
	}
}
//Moves the Slider's Box after the mouse.
void Slider::MoveBox(){
	int x_pos;
	int y_pos;
	int center_of_box_x = 0;;
	int box_width;
	box_width  = 0;
	SDL_GetMouseState(&x_pos,&y_pos);
	if (x_pos != center_of_box_x)
	{
		this->mouse_pointer_offset = center_of_box_x - x_pos;
	}
}