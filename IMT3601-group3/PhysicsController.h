#pragma once
#include <Box2D/Box2D.h>
#include "Scenery.h"
#include <vector>
#include "btdContactListener.h"
#include "Dictionary.h"

class PhysicsController
{
private:
	/*The Box2D world - this should be changed!*/
	b2Vec2 btdGravity;
	b2World* btdWorld;
	/*The Box2D ground - this should be changed!*/
	b2BodyDef btdGroundBodyDef;
	b2Body* btdGroundBody;
	b2PolygonShape btdGroundBox;
	/*Box2D - this should be changed!*/
	int32 velocityIterations;
	int32 positionIterations;
	/*Box2d stage*/
	int width_;
	int height_;
	std::vector<Scenery> platforms_;
	/*Box2d contact listener*/
	BtdContactListener *contact_listener_;
	//Time items.
	float deltaTime_;
	int lastTime_;
	int thisTime_;
public:
	//Empty constructor.
	PhysicsController();
	//Costructor that takes the width and height of the world.
	PhysicsController(int,int);
	//Constructor that takes the width and height of the world followed by a vector of Scenery objects.
	PhysicsController(int,int,std::vector<Scenery>);
	~PhysicsController();
	//Adds a Box2D body to the world.
	b2Body* AddBody(b2BodyDef*);
	//Creates a platform and adds it to the world.
	b2Body* CreatePlatform(int,int,int,int);
	//Returns the foot collisions of the index.
	int get_foot_sensor_collisions(int);
	//Generates a world with a worldbox, width and height.
	void generate_world_box(b2Body*,int,int);
	//Creates a Box2D box starting in the top left corner with x and y, followed by the width and height.
	void CreateBox(b2Body*,float,float,float,float);
	//Fills a vector with all hit events that have happened in the last frame.
	void GetCharHitEvents(std::vector<CharHitEvent>&);
	//Updates the world with one time step.
	void updateWorld(void);
};

