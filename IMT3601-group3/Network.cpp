#include "Network.h"

Network* Network::network_instance_ = NULL;

Network::Network(Uint16 client_port, char *server_name, Uint16 server_port, bool isHost)
{
	is_host_ = isHost;
	stage_id_ = -1;
	characters_ = 1;
	character_id_ = 0;
	character_type_ = -1;
	shot_factory_ = nullptr;
	physics_controller_ = nullptr;

	/* Initialize SDL_net */
	if (SDLNet_Init() < 0)
	{
		fprintf(stderr, "SDLNet_Init: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}

	/* Check for parameters	if there are three overwrite otherwise we will be using default values */
	if (server_name) {
		server_name_ = server_name;
	} else {
		server_name_ = "127.000.000.001";
		fprintf(stderr, "Usage: udpclient host adress (default: updclient %s ) \n", server_name_);
	}
	if (server_port) {
		server_port_ = server_port;
	} else {
		server_port_ = default_server_port_;
		fprintf(stderr, "Usage: udpclient host port (default: updclient %d ) \n", server_port_);
	}
	if (client_port) {
		client_port_ = client_port;
	} else {
		client_port_ = default_client_port_;
		fprintf(stderr, "Using default portnumber %d.", client_port_);
	}

	/* Open a socket */
	if (is_host_) {
		client_port_ = server_port_;
	}
	if (!(socket_ = SDLNet_UDP_Open(client_port_))) {
		fprintf(stderr, "SDLNet_UDP_Open: %s\n", SDLNet_GetError());
		if (!(socket_ = SDLNet_UDP_Open(rand()%9999))) {
			fprintf(stderr, "SDLNet_UDP_Open: %s\n", SDLNet_GetError());		// !!!
			exit(EXIT_FAILURE);
		}
	}

	/* Open a socket on random port */
	/*if (!(socket_ = SDLNet_UDP_Open(0))) {
		fprintf(stderr, "SDLNet_UDP_Open: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}*/

	/* Resolve server name into server address */
	if (SDLNet_ResolveHost(&server_address_, server_name_, server_port_) == -1) {
		fprintf(stderr, "SDLNet_ResolveHost(%s %d): %s\n",server_name_ ,server_port_, SDLNet_GetError());
		exit(EXIT_FAILURE);
	}

	/* Make space for the packet */
	if (!(packet_ = SDLNet_AllocPacket(packet_size_))) {
		fprintf(stderr, "SDLNet_AllocPacket: %s\n", SDLNet_GetError());
		exit(EXIT_FAILURE);
	}
}

Network::~Network(void)
{
	SDLNet_FreePacket(packet_);
	SDLNet_Quit();
}

/*Uint64 Network::get_packet_input(void)
{
	//if (SDLNet_UDP_Recv(server_socket_, server_packet_))
	SDLNet_UDP_Recv(socket_, packet_);
	Uint8 stuff[64/8];
	Uint64 data;
	for (int i = 0; i < 64/8; i++)
	{
		stuff[i]= packet_->data[i];
		data = stuff[i];
		data << 4;
	}
	return data;
}*/

void Network::get_packet(void) {
	while(SDLNet_UDP_Recv(socket_, packet_))
	{
		if (is_host_) {
			if (packet_->data[8] == PLAYER_INPUT_PACKET) {
				int id = packet_->data[9];
				Uint32 input = packet_->data[10];
				input += ((Uint32)packet_->data[11] << 8);
				input += ((Uint32)packet_->data[12] << 16);
				input += ((Uint32)packet_->data[13] << 24);
				Uint16 angle = packet_->data[14];
				angle += ((Uint16)packet_->data[15] << 8);

				if(stage_ != nullptr && id < stage_->agents_.size()) {
					stage_->agents_[id].InputUpdate(input, angle);
					connection_list_[id-1].ticks_last_message_ = SDL_GetTicks();
				}
			} else if (packet_->data[8] == CLIENT_HANDSHAKE_PACKET) {
				IPaddress temp_ip = packet_->address;
				int temp_int = packet_->data[packet_->len];
				
				if(add_client(packet_->address)) { 
					b2Vec2 spawnpoint = b2Vec2(200,200); // !!!

					int type = packet_->data[9];
					ClientConnection new_connection = {packet_->address, SDL_GetTicks()};
					connection_list_.push_back(new_connection);

					stage_->agents_.push_back(Character(spawnpoint.x, spawnpoint.y, physics_controller_, packet_->data[9], stage_->agents_.size(), shot_factory_));
					server_handshake(connection_list_.back().address, stage_->agents_.size()-1, stage_id_);
				}
				verify_packet(temp_ip, temp_int);

				printf("\n %d", CLIENT_HANDSHAKE_PACKET);

			} else if (packet_->data[8]  == BULLET_POSITION_PACKET){
				packet_->len = 15;
	
				int id = packet_->data[9];					// bullet id
				int pos_x = packet_->data[10];				// x position 0-65536
				pos_x += (packet_->data[11] << 8);			//
				int pos_y_ = packet_->data[12];				// y position 0-65536
				pos_y_ = (packet_->data[13] << 8);			//
	
				for (int i = 0; i < connection_list_.size(); i++) {				// for all clients 
					packet_->address = connection_list_[i].address;

					if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL){
						printf("Error in sending packet\n");
					}
				}
			}
		} else {

			if (packet_->data[8] == CHARACTER_POSITION_PACKET) {
				int character_id = packet_->data[9];

				int pos_x = packet_->data[10] + ((Uint16)packet_->data[11] << 8);
				int pos_y = packet_->data[12] + ((Uint16)packet_->data[13] << 8);

				double angle = packet_->data[14];							// Angle 0-256
				angle /= 256.0;												// Angle 0-1
				angle *= 360.0;												// Angle 0-360

				if(stage_ != nullptr && character_id < stage_->agents_.size()) {
					
					if(character_id == LOCAL_PLAYER_NUMBER && character_id_ < stage_->agents_.size()) {
						stage_->agents_[character_id_].set_x_pos(pos_x);					
						stage_->agents_[character_id_].set_y_pos(pos_y);	
						stage_->agents_[character_id_].set_rotation(angle);

					} else if(character_id == character_id_) {
						stage_->agents_[LOCAL_PLAYER_NUMBER].set_x_pos(pos_x);					
						stage_->agents_[LOCAL_PLAYER_NUMBER].set_y_pos(pos_y);	
						stage_->agents_[LOCAL_PLAYER_NUMBER].set_rotation(angle);

					} else {
						stage_->agents_[character_id].set_x_pos(pos_x);			
						stage_->agents_[character_id].set_y_pos(pos_y);	
						stage_->agents_[character_id].set_rotation(angle);

					}
				}

			} else if (packet_->data[8]  == SERVER_HANDSHAKE_PACKET) {
				IPaddress temp_ip = packet_->address;
				int temp_int = packet_->data[packet_->len];

				character_id_ = packet_->data[9];
				stage_id_ = packet_->data[10];
				characters_ = packet_->data[11];

				verify_packet(temp_ip, temp_int);

			} else if (packet_->data[8] == CLIENT_UPDATE_PACKET) {
				IPaddress temp_ip = packet_->address;
				int temp_int = packet_->data[packet_->len];

				int id = packet_->data[9];
				int type = character_type_ = packet_->data[10];
				int action = packet_->data[11];

				if (stage_ != nullptr && id == stage_->agents_.size()) {
					b2Vec2 spawnpoint = b2Vec2(200,200); // !!!
					stage_->agents_.push_back(Character(spawnpoint.x, spawnpoint.y, physics_controller_, type, stage_->agents_.size(), shot_factory_));
				}
			}
		}
		if (packet_->data[8] == CRITICAL_VERIFICATION_PACKET) {
			for (int i = 0; i < critical_packets_.size(); i++) {
				if (packet_->data[9] == critical_packets_[i].data[critical_packets_[i].len] && 
					packet_->address.host == critical_packets_[i].address.host && 
					packet_->address.port == critical_packets_[i].address.port) {
					critical_packets_.erase(critical_packets_.begin() + i);
					i--;
				}
			}
				printf("\n %d", CRITICAL_VERIFICATION_PACKET);
		}
	}
}

void Network::verify_packet(IPaddress ip, int id) {
	packet_->data[8] = CRITICAL_VERIFICATION_PACKET;
	packet_->data[9] = id;

	packet_->address = ip;

	if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL)
	{
		printf("Error in sending packet\n");
	}
	printf("verify packet\n");
}


void Network::send_input_packet(Uint32 input, double rotation) {
	packet_->len = 15;

	int angle = rotation*100;
	angle += 36000;
	angle %= 36000;
	
	packet_->data[8]  = (Uint8)PLAYER_INPUT_PACKET;					// packet type identifier
	packet_->data[9]  = (Uint8)character_id_;						// character id
	packet_->data[10] = (Uint8)(input & 0x000000ff);				// keyboard and mouse input
	packet_->data[11] = (Uint8)((input & 0x0000ff00) >> 8);			// 
	packet_->data[12] = (Uint8)((input & 0x00ff0000) >> 16);		// 
	packet_->data[13] = (Uint8)((input & 0xff000000) >> 24);		// 
	packet_->data[14] = (Uint8)((Uint16)angle & 0x00ff);			// character aiming angle
	packet_->data[15] = (Uint8)(((Uint16)angle & 0xff00) >> 8);		// 
	
	packet_->address = server_address_;

	if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL)
	{
		printf("Error in sending packet\n");
	}
}

void Network::send_character_positions(int character_id, int pos_x, int pos_y, double rotation) {
	packet_->len = 15;

	int angle = (int)(rotation + 360);							// Angle 0-360
	angle %= 360;
	angle = ((rotation/360)*256);								// Between 0 and 256
	
	packet_->data[8]  = (Uint8)CHARACTER_POSITION_PACKET;		// packet type identifier 2 = character position
	packet_->data[9]  = (Uint8)character_id;					// character id
	packet_->data[10] = (Uint8)(pos_x & 0x00ff);				// x position 0-65536
	packet_->data[11] = (Uint8)((pos_x & 0xff00) >> 8);			//
	packet_->data[12] = (Uint8)(pos_y & 0x00ff);				// y position 0-65536
	packet_->data[13] = (Uint8)((pos_y & 0xff00) >> 8);			//
	packet_->data[14] = (Uint8)angle;							// character aim rotation 
	
	for (int i = 0; i < connection_list_.size(); i++) {				// for all clients 
		packet_->address = connection_list_[i].address;

		if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL){
			printf("Error in sending packet\n");
		}
	}
}

void Network::send_bullet_positions(int bullet_id, int pos_x, int pos_y) {
	packet_->len = 15;
	
	packet_->data[8]  = (Uint8)BULLET_POSITION_PACKET;			// packet type identifier 2
	packet_->data[9]  = (Uint8)bullet_id;						// bullet id
	packet_->data[10] = (Uint8)(pos_x & 0x00ff);				// x position 0-65536
	packet_->data[11] = (Uint8)((pos_x & 0xff00) >> 8);			//
	packet_->data[12] = (Uint8)(pos_y & 0x00ff);				// y position 0-65536
	packet_->data[13] = (Uint8)((pos_y & 0xff00) >> 8);			//
	
	for (int i = 0; i < connection_list_.size(); i++) {				// for all clients 
		packet_->address = connection_list_[i].address;

		if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL){
			printf("Error in sending packet\n");
		}
	}
}

void Network::client_handshake(Uint8 character_type) {
	packet_->len = 15;
	
	packet_->data[8] = (Uint8)CLIENT_HANDSHAKE_PACKET;
	packet_->data[9] = character_type;
	packet_->data[packet_->len]  = (Uint8)++critical_packet_id_;

	packet_->address = server_address_;

	critical_packets_.push_back(*packet_);
}

void Network::server_handshake(IPaddress client, int character_id, int stage_id) {
	packet_->len = 15;

	packet_->data[8]  = (Uint8)SERVER_HANDSHAKE_PACKET;	
	packet_->data[9]  = (Uint8)character_id;
	packet_->data[10] = (Uint8)stage_id;	
	packet_->data[11] = (Uint8)characters_;
	packet_->data[packet_->len]  = (Uint8)++critical_packet_id_;

	packet_->address = client;

	critical_packets_.push_back(*packet_);
	send_critical_packets();
}

void Network::send_critical_packets(){
	for (int i = 0; i < critical_packets_.size(); i++)
	{
		*packet_ = critical_packets_[i];
		if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL)
		{
			printf("Error in sending packet\n");
		}
	}
	
}

void Network::client_close_connection() {						// client message to server (quit)

	packet_->len = 15;
	
	packet_->data[8]  = (Uint8)CLIENT_CLOSE_CONNECTION_PACKET;	// packet type identifier
	packet_->data[9]  = (Uint8)character_id_;					// character id

	packet_->address = server_address_;

	if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL)
	{
		printf("Error in sending packet\n");
	}
}

void Network::server_close_connection(int character_id) {						// server message to client 
	packet_->len = 15;
	
	packet_->data[8]  = (Uint8)SERVER_CLOSE_CONNECTION_PACKET;	// packet type identifier
	packet_->data[9]  = (Uint8)character_id;								// character id, 0 = host

	for (int i = 0; i < connection_list_.size(); i++) {
		packet_->address = server_address_;
		
		if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL)
		{
			printf("Error in sending packet\n");
		}
	}
	printf("\n Connection timed out for: %d.", character_id);
	
}

bool Network::get_is_server() {
	if (network_instance_ == NULL) { 
		return true; 
	}
	return is_host_;
}

void Network::client_update() {										// client handshake 
	packet_->len = 15;
	
	packet_->data[8] = (Uint8)CLIENT_UPDATE_PACKET;					// packet type identifier
	packet_->data[11] = 0;											//nothing, create, delete

	for (int i = 0; i < connection_list_.size(); i++) {
		for (int j = 0; j < stage_->agents_.size(); j++)
		{
			packet_->data[9] = j;
			packet_->data[10] = stage_->agents_[j].get_character_type();;
			packet_->data[packet_->len]  = (Uint8)++critical_packet_id_;

			packet_->address = connection_list_[i].address;

			if (SDLNet_UDP_Send(socket_,-1,packet_) == NULL) {
				printf("Error in sending packet\n");
			} 
		}
	}
	
}

int Network::get_characters() {
	return characters_;
}
int Network::get_stage() {
	return stage_id_;
}
void Network::set_stage_id(int selected_stage) {
	stage_id_ = selected_stage;
}

int Network::get_character_id() {
	return character_id_;
}
void Network::client_status_check() {
	for (int i = 0; i < connection_list_.size(); i++) {
		if((SDL_GetTicks() - connection_list_[i].ticks_last_message_) < 10000) {
			server_close_connection(i);
		}
	}
}

void Network::set_externals(stage_objects* stage, ShotFactory* factory, PhysicsController* physics_controller) {
	stage_ = stage;
	shot_factory_ = factory;
	physics_controller_ = physics_controller;
}

bool Network::add_client(IPaddress ip) {
	for (int i = 0; i < connection_list_.size(); i++){
		if (connection_list_[i].address.host == ip.host && 
			connection_list_[i].address.port == ip.port) {
			printf("add client false");
			return false;
		}
		printf("add client true");
	}
	return true;
}

Network* Network::get_instance() {
	#pragma omp critical
	{
		if (network_instance_ == NULL)
		{		
			printf("Error: no default constructor for network");
		}
	}

	return network_instance_;
}

Network* Network::get_instance(Uint16 client_port, char *server_name, Uint16 server_port, bool isHost) {
	#pragma omp critical
	{
		if (network_instance_ == NULL) {		
			network_instance_ = new Network(client_port, server_name, server_port, isHost);	
		} else if (network_instance_->get_is_server() != isHost){
			network_instance_ = new Network(client_port, server_name, server_port, isHost);	
			printf("\nWarning: network instance overwritten! (host/client switch)");
		}
	}

	return network_instance_;
}

