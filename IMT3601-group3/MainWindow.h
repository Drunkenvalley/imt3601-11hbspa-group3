#pragma once
#include "sdl.h"
#include <string>
using namespace std;
class MainWindow
{
private:
	//Private constructors.
	MainWindow();
	MainWindow(MainWindow const&);
	void operator=(MainWindow const&);
	//Static pointer to the window object.
	static MainWindow* main_window_instance_;
	//Pointer to the current SDL window.
	SDL_Window *main_window_object_;
public:
	~MainWindow(void);
	//get instance of class
	static MainWindow* getMainWindow();
	//get the contained window
	SDL_Window* get_main_window_object();
	//public constructor for the contained window object
	bool makeWindow(string name,int pos_x,int pos_y,int width,int height,Uint32 screen_flags);
	//set window size and exit fullscreen
	void setSize(int,int);
	// set fullscreen mode
	void setSize();
};


