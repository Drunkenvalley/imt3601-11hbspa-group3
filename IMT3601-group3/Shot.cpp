#include "Shot.h"

#define DEAD_ZONE -50.0f,-50.0f
//Emptyu constructor that takes no arguments but sends default
// argument to the GraphicsAsset.
Shot::Shot():GraphicsAsset(DEAD_ZONE, AMMO_NAME_ONE)
{
	weapon_ = false;
	//Tracker for how long the bullet has been active.
	time_ = SDL_GetTicks();
}
//Constructor that takes the starting position and string to the 
// sprite that the Shot have.
Shot::Shot(int x_pos,int y_pos, string file_name):GraphicsAsset(x_pos,y_pos,file_name)
{
	//Tracker for how long the bullet has been active.
	time_ = SDL_GetTicks();
}
//Copy constructor.
Shot::Shot(const Shot& other):GraphicsAsset(other)
{
	*this = other;
}

Shot::~Shot(void)
{
}
//Draws the Shot in the currently acctive SDL Renderer.
void Shot::Draw()
{
	if ( bullet_->IsActive()) {
		sprites_->DrawSprite(image_id_,pos_x_,pos_y_);
	}
}
//Sets the body, this is neccecary if you want to use the Shot actively in the game.
void Shot::set_body(b2Body* body)
{
	bullet_ = body;
	bullet_->SetSleepingAllowed(true);
}
//Destroys the Box2D body from the game.
void Shot::destroyBullet()
{
	bullet_->GetWorld()->DestroyBody(bullet_);
	bullet_ = nullptr;
}
//Updates the Shot to the current time step.
void Shot::Update()
{
	if(bullet_ && bullet_ != (b2Body*) 0xfeeefeee){
		b2Vec2 position = bullet_->GetPosition();
		pos_x_ = position.x*BOX_2D_SCALING;
		pos_y_ = position.y*BOX_2D_SCALING;
	}
}
//Boolean check to se if the bullet is in motion.
bool Shot::IsMoving()
{
	//if(bullet_ != (b2Body*) 0xfeeefeee){ // keeping this for debug if vectors does something weird
	return bullet_->IsAwake();
	//}
}
//Returns millisecond count since the Shot was last activated.
int Shot::get_alive_time()
{
	int delta = SDL_GetTicks();
	delta -= time_;
	return delta;
}
//Moves the Box2D body to the new position.
void Shot::set_body_pos(const int x_pos,const int y_pos)
{
	bullet_->SetTransform(b2Vec2(x_pos/BOX_2D_SCALING,y_pos/BOX_2D_SCALING),bullet_->GetAngle());
}
//Deactivates the Box2D body, this would have disabled all collision
// detection if Box2D wasn't a bitch!
void Shot::Deactivate()
{
	b2Filter filter;
	filter = bullet_->GetFixtureList()->GetFilterData();
	filter.groupIndex = -8;
	bullet_->GetFixtureList()->SetFilterData(filter);

	bullet_->SetAwake(false);
	bullet_->SetActive(false);
}
//Reactivates the bullet and resets the time counter.
void Shot::Activate(void)
{
	b2Filter filter;
	filter = bullet_->GetFixtureList()->GetFilterData();
	filter.groupIndex = 0;
	bullet_->GetFixtureList()->SetFilterData(filter);
	bullet_->SetActive(true);
	bullet_->SetAwake(true);
	time_ = SDL_GetTicks();
}
//Sets the velocity of the Shot.
void Shot::set_velocity(b2Vec2 velocity)
{
	bullet_->SetLinearVelocity(velocity);
}
//Returns the Box2D ID of the Shot.
int Shot::get_btd_id(){
	void* raw_data = this->bullet_->GetFixtureList()[0].GetUserData();
	int data = (int) raw_data;
	return data;
};

bool Shot::is_weapon(){
	return weapon_;

}

void Shot::set_weapon(bool as_param){
weapon_ = as_param;
}

void Shot::reset_image_id(){
	set_image_id(sprites_->get_sprite_id(AMMO_NAME_ONE));
};


