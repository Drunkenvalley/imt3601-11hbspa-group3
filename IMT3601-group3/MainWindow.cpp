#include "MainWindow.h"

//NULLs the instance for lazy instantiation.
MainWindow* MainWindow::main_window_instance_ = NULL;

MainWindow::MainWindow(void)
{
}


MainWindow::~MainWindow(void)
{
	SDL_DestroyWindow(main_window_object_);
}
//Returns a poitner to the the singelton Renderer.
MainWindow* MainWindow::getMainWindow()
{
#pragma omp critical
	{
		if (main_window_instance_ == NULL)
		{		
			main_window_instance_ = new MainWindow();	
		}
	}
	return main_window_instance_;
}
//Returns a pointer to the currently acctive SDL Renderer.
SDL_Window* MainWindow::get_main_window_object(){
	return main_window_object_;
};

bool MainWindow::makeWindow(string name,int pos_x,int pos_y,int width,int height,Uint32 screen_flags){				
	main_window_object_ = SDL_CreateWindow(name.c_str(), pos_x, pos_y, width, height, screen_flags);
	if (main_window_object_ == nullptr){
		return false;
	}
	 
	return true;
}

void MainWindow::setSize(int width, int height){
		SDL_SetWindowFullscreen(main_window_object_,0);

	SDL_SetWindowSize(main_window_object_,width,height);
};
void MainWindow::setSize(){
		//SDL_SetWindowSize(main_window_object_,800,600);

	SDL_SetWindowFullscreen(main_window_object_,SDL_WINDOW_FULLSCREEN);

};
