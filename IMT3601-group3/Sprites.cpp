#include "Sprites.h"
#include "Renderer.h"

//Null object for lazy instantiation of the Singelton.
Sprites* Sprites::sprite_object_ = NULL;
//Constructor that takes no arguments and setts the SDL renderer.
Sprites::Sprites(){
	setRenderer();
};
//Singelton	return function of the Sprites object.
Sprites* Sprites::getSpritesInstance(void){
#pragma omp critical
	{
		if (sprite_object_ == NULL)
		{		
			sprite_object_ = new Sprites();	
			sprite_object_->setRenderer();
		}
	}

	return sprite_object_;
}

Sprites::~Sprites(void)
{
}

//Find the index of the image, if not found, try to load it.
//Return -1 if not found
int Sprites::get_sprite_id(std::string kName) {
	//Loops trough the vector of images looking for a image with the same name.
	for (int i = 0; i < image_names_.size(); i++){
		if(kName == image_names_[i]) return i;
	}
	//Loads and returns the image index.
	if (this->LoadImage(kName)){
		return images_.size()-1;
	}
	return -1;
}

//Get the width of the picture.
int Sprites::get_width(int kIndex) {
	int width = 0;
	int height = 0;
	if (kIndex != -1)
	{
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
	}
	return width;
}

//Get the height of the picture.
int Sprites::get_height(int kIndex) {
	int width = 0;
	int height = 0;
	if (kIndex != -1)
	{
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
	}
	return height;
}
//Loads a image, true on success, false on failure.
bool Sprites::LoadImage(std::string kFilepath){
	SDL_Texture* load = nullptr;
	load = IMG_LoadTexture(renderer_, kFilepath.c_str());
	if (load == nullptr ){
		printf("Error: Failed to load image %s\n", kFilepath.c_str());
		return false;
	}
	images_.push_back(load);
	image_names_.push_back(kFilepath);
	return true;
}

//Draw the sprite with the given index at x, y.
//Returns true on success.
bool Sprites::DrawStationary(int kIndex, float x, float y) {
	int width = 0;
	int height = 0;
	Camera* camera_ptr = Camera::getCamera();
	double scaleRezolutionX = camera_ptr->get_screen_width()/DEFAULT_SCREEN_WIDTH;
	double scaleRezolutionY = camera_ptr->get_screen_height()/DEFAULT_SCREEN_HEIGHT;
	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		source_rectangle_.x = 0; 
		source_rectangle_.y = 0; 
		source_rectangle_.w = width; 
		source_rectangle_.h = height;
		destination_rectangle_.x = x - get_width(kIndex)/2;
		destination_rectangle_.y = y - get_height(kIndex)/2;
		destination_rectangle_.w = width;
		destination_rectangle_.h = height;

		destination_rectangle_.x *= scaleRezolutionX;
		destination_rectangle_.y *= scaleRezolutionY;
		destination_rectangle_.w *= scaleRezolutionX;
		destination_rectangle_.h *= scaleRezolutionY;

		SDL_RenderCopy(renderer_, images_[kIndex], &source_rectangle_, &destination_rectangle_);
		return true;
	}
	return false;
}
bool Sprites::DrawAsBackground(int kIndex) {
	int width = 0;
	int height = 0;
	Camera* camera_ptr = Camera::getCamera();
	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		source_rectangle_.x = 0; 
		source_rectangle_.y = 0; 
		source_rectangle_.w = width; 
		source_rectangle_.h = height;
		destination_rectangle_.x = 0;
		destination_rectangle_.y = 0;
		destination_rectangle_.w = camera_ptr->get_screen_width();
		destination_rectangle_.h = camera_ptr->get_screen_height();

			SDL_RenderCopy(renderer_, images_[kIndex], &source_rectangle_, &destination_rectangle_);
	return true;
	}
	return false;
}

//Draw the sprite with the given index at x, y.
//Returns true on success.
bool Sprites::DrawSprite(int kIndex, float x, float y) {
	int width = 0;
	int height = 0;

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		SDL_Rect source; source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;

		SDL_Rect sprite; 
		sprite.x = x - get_width(kIndex)/2; 
		sprite.y = y - get_height(kIndex)/2; 
		sprite.w = width; 
		sprite.h = height;

		SDL_RenderCopy(renderer_, images_[kIndex], &source, &sprite);
		return true;
	}
	return false;
}

//Draws a sprite at x,y with a scaling in x.
bool Sprites::DrawSprite(int kIndex, float x, float y,float x_scale) {
	int width = 0;
	int height = 0;

	if(x_scale == 0){
		return false;
	}

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		SDL_Rect source; 
		source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;

		SDL_Rect dest;
		dest.x = x - get_width(kIndex)/2; 
		dest.y = y - get_height(kIndex)/2; 
		dest.w = width*x_scale/100.0f; 
		dest.h = height;

		SDL_RenderCopy(renderer_, images_[kIndex], &source, &dest);
		return true;
	}
	return false;
}
//Draws a sprite at x,y with inverted scaling in x.
bool Sprites::DrawSpriteVerticalBar_inverted(int kIndex, float x, float y,float y_scale) {
	int width = 0;
	int height = 0;

	if(y_scale == 100){ //100 procent		return false;
	}

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		SDL_Rect source; 
		source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;

		SDL_Rect dest;
		dest.x = x - get_width(kIndex)/2; 
		dest.y = y - get_height(kIndex)/2; 
		dest.w = width; 
		dest.h = height*((100-y_scale)/100.0f);

		SDL_RenderCopy(renderer_, images_[kIndex], &source, &dest);
		return true;
	}
	return false;
}

//Draws a sprite at x,y with a rotation.
bool Sprites::DrawSprite(int kIndex, float x, float y, double rotation) {
	int width = 0;
	int height = 0;

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	SDL_Point center;
	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);

		SDL_Rect source; 
		source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;

		SDL_Rect sprite; 
		sprite.x = x - get_width(kIndex)/2; 
		sprite.y = y - get_height(kIndex)/2; 
		sprite.w = width; 
		sprite.h = height;

		center.x = sprite.w/2;
		center.y = sprite.h/2;

		SDL_RenderCopyEx(renderer_, images_[kIndex], &source, &sprite,rotation,&center,SDL_FLIP_NONE);
		return true;
	}
	return false;
}
//Draws a sprite at	x,y with a rotation centered around a SDL_Point.
//Returns true on success.
bool Sprites::DrawSprite(int kIndex, float x, float y, double rotation, SDL_Point center) {
	int width = 0;
	int height = 0;

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		
		SDL_Rect source; source.x = 0;
		source.y = 0; 
		source.w = width;
		source.h = height;
		
		SDL_Rect sprite;
		sprite.x = x - get_width(kIndex)/2;
		sprite.y = y - get_height(kIndex);
		sprite.w = width; 
		sprite.h = height;

		SDL_RenderCopyEx(renderer_, images_[kIndex], &source, &sprite,rotation,&center,SDL_FLIP_NONE);
		return true;
	}
	return false;
}
//Draws a sprite at x,y with a rotation and flipping along the horizontal and/or
// vertical axes. Returns true on success.
bool Sprites::DrawSprite(int kIndex, float x, float y, double rotation,SDL_RendererFlip flip_flags) {
	int width = 0;
	int height = 0;
	SDL_Point center;

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		SDL_Rect source; 
		source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;
		
		SDL_Rect sprite; 
		sprite.x = x - get_width(kIndex)/2; 
		sprite.y = y - get_height(kIndex)/2; 
		sprite.w = width; 
		sprite.h = height; 

		SDL_RenderCopyEx(renderer_, images_[kIndex], &source, &sprite,rotation,&center,flip_flags);
		return true;
	}
	return false;
}
//Draws a sprite at x,y with a rotation around a SDL_Point center and SDL RenderFlip around the 
// horizontal and/or vertical axes. Returns true on success.
bool Sprites::DrawSprite(int kIndex, float x, float y, double rotation,SDL_Point center,SDL_RendererFlip flip_flags) {
	int width = 0;
	int height = 0;

	Camera* camera_ptr = Camera::getCamera();
	x = x - camera_ptr->get_x();
	y = y - camera_ptr->get_y();

	if(kIndex != -1){
		SDL_QueryTexture(images_[kIndex], NULL, NULL, &width, &height);
		
		SDL_Rect source; 
		source.x = 0; 
		source.y = 0; 
		source.w = width; 
		source.h = height;
		
		SDL_Rect sprite; 
		sprite.x = x - get_width(kIndex)/2; 
		sprite.y = y - get_height(kIndex); 
		sprite.w = width; 
		sprite.h = height;

		if(flip_flags == SDL_FLIP_NONE) {
			SDL_RenderCopyEx(renderer_, images_[kIndex], &source, &sprite,rotation,&center,flip_flags);
		} else {
			SDL_RenderCopyEx(renderer_, images_[kIndex], &source, &sprite,rotation-180,&center,flip_flags);
		}
		return true;
	}
	return false;
}
//Sets the SDL renderer to the currently active renderer.
void Sprites::setRenderer(){
	Renderer* renderer = Renderer::getRenderer();
	renderer_ = renderer->get_renderer_object();
}
//Returns the filestring to a Sprite.
std::string Sprites::get_name(int id) {
	return image_names_[id];
}