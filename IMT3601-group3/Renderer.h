#pragma once
#include <SDL.h>
class Renderer
{
private:
	//Private constructors.
	Renderer();
	Renderer(Renderer const&);
	void operator=(Renderer const&);
	//Static pointer to the Renderer object.
	static Renderer* renderer_instance_;
	//Pointer to the current SDL Renderer.
	SDL_Renderer *renderer_object;
public:
	static Renderer* getRenderer();
	//Returns a pointer to the current active SDL renderer.
	SDL_Renderer* get_renderer_object(); 
	//Methods to set active SDL Renderer.
	void setRenderer(SDL_Renderer *renderer);
	void setRenderer(SDL_Renderer **renderer);
	void setRenderer(SDL_Window* window, int index, Uint32 flags);
};