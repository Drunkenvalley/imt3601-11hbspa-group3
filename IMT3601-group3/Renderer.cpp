#include "Renderer.h"
#include <omp.h>
//NULLs the instance for lazy instantiation.
Renderer* Renderer::renderer_instance_ = NULL;
//Empty constructor.
Renderer::Renderer(){
}
//Returns a poitner to the the singelton Renderer.
Renderer* Renderer::getRenderer()
{
#pragma omp critical
	{
		if (renderer_instance_ == NULL)
		{		
			renderer_instance_ = new Renderer();	
		}
	}
	return renderer_instance_;
}
//Returns a pointer to the currently acctive SDL Renderer.
SDL_Renderer* Renderer::get_renderer_object(){
	return renderer_object;
};
//Sets the SDL Renderer to passed renderer.
void Renderer::setRenderer(SDL_Renderer *renderer)
{
	renderer = this->renderer_object;
}
//Sets the SDL Renderer to passed renderer.
void Renderer::setRenderer(SDL_Renderer **renderer)
{
	renderer = &this->renderer_object;
}
//Sets the SDL Renderer to a new renderer.
void Renderer::setRenderer(SDL_Window* window, int index, Uint32 flags)
{
#pragma omp critical
	{
	if (renderer_object){
		SDL_DestroyRenderer(renderer_object);
	}

	renderer_object = SDL_CreateRenderer(window, index, flags);
	}	
}