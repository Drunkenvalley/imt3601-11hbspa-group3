#pragma once
#include <Box2D\Box2D.h>
#include "Dictionary.h"
#include <vector>

class BtdContactListener:
	public b2ContactListener
{
private:
	//Vector of hits detected on a character.
	std::vector<CharHitEvent> char_hit_event_list_;
	//Member variable to reduce memory access.
	CharHitEvent temp_char_hit_event_;
	//Array of collisions.
	int foot_sensor_collisions_[NUMBER_OF_PLAYERS];
public:
	//Empty constructor.
	BtdContactListener();
	~BtdContactListener();
	//Returns true if sent index has a undealt hit detection.
	int get_foot_sensor_collisions(int);
	//Fills the sent vector with Character Hit Events that happened last timeframe.
	void GetCharHitEvents(std::vector<CharHitEvent>&);
	//Registers the beginning of contact between two objects.
	void BeginContact(b2Contact*);
	//Registers the end of contact between two objects.
	void EndContact(b2Contact*);
};

